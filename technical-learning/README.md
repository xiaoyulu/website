# ICT 技术学习

## 1~50

1. 机器学习基础 - 吴恩达，[B站](https://www.bilibili.com/video/BV164411b7dx)
2. 深度学习基础 - 吴恩达，[B站](https://www.bilibili.com/video/BV1FT4y1E74V)
3. Pytorch - 深度学习框架，[B站](https://www.bilibili.com/video/BV1Rv411y7oE)
4. Tensorflow - 深度学习框架，[B站](https://www.bilibili.com/video/BV1kW411W7pZ)
5. 线性代数 - [B站](https://www.bilibili.com/video/BV1at411d79w)
6. 计算机原理 - [B站](https://www.bilibili.com/video/av21376839)
7. 操作系统 - 清华大学，[B站](https://www.bilibili.com/video/BV1js411b7vg)
8. 数据结构 - 浙江大学，[B站](https://www.bilibili.com/video/BV1JW411i731)
9. [昇腾 AI 社区](https://www.hiascend.com/)
10. [华为开发者学堂](https://developer.huawei.com/consumer/cn/training/)
11. [百度技术培训中心](https://bit.baidu.com/index) - 是百度技术的布道者和赋能者。
12. [阿里云开发者社区](https://developer.aliyun.com/)
13. [网站建设学习路线](https://developer.huaweicloud.com/resource/roadmap/website.html) - 华为。涵盖 HTML、CSS、JavaScript、ajax、vue、node、typescript、angular。
14. [java 学习路线](https://developer.huaweicloud.com/resource/roadmap/java.html) - 华为。
15. [物联网学习路线](https://developer.huaweicloud.com/resource/roadmap/iot.html) - 华为。物联网一站式学习平台 ，涵盖物联网理论基础、嵌入式开发、应用开发、大数据分析从零开始，快速提升开发技能！
16. [AI 学习路线](https://developer.huaweicloud.com/resource/roadmap/ai.html) - 华为。
17. [数据库学习路线](https://developer.huaweicloud.com/resource/roadmap/db.html) - 华为。
18. [移动开发学习路线](https://developer.huaweicloud.com/resource/roadmap/app.html) - 华为。
19. [系统运维学习路线](https://developer.huaweicloud.com/resource/roadmap/ops.html) - 华为。
20. [大数据学习路线](https://developer.huaweicloud.com/resource/roadmap/data.html) - 华为。
21. [python 学习路线](https://developer.huaweicloud.com/resource/roadmap/python.html) - 华为。
22. [C 语言学习路线](https://developer.huaweicloud.com/resource/roadmap/c.html) - 华为。
23. [阿里云大学-数据库学习路线](https://gitee.com/xiaoyulu/website/wikis/?sort_id=2035783)
24. [阿里云大学-Linux 运维学习路线](https://gitee.com/xiaoyulu/website/wikis/?sort_id=2035807)
25. [阿里云大学-Java学习路线](https://gitee.com/xiaoyulu/website/wikis/?sort_id=2035814)
26. [阿里云大学-前端开发学习路线](https://gitee.com/xiaoyulu/website/wikis/?sort_id=2035846)
27. [阿里云大学-Python学习路线](https://gitee.com/xiaoyulu/website/wikis/?sort_id=2035848)
28. [阿里云大学-学习 jQuery UI](https://edu.aliyun.com/course/488?spm=5176.10731491.0.0.82I8M9)
29. [阿里云大学-正则表达式入门教程](https://edu.aliyun.com/course/494?spm=5176.10731491.0.0.82I8M9)
30. [阿里云大学-JavaScript 自学手册](https://edu.aliyun.com/course/473?spm=5176.10731491.0.0.JkZ1Yx)
31. [阿里云大学-Vue.js完全自学手册](https://edu.aliyun.com/course/493?spm=5176.8764728.aliyun-edu-course-tab.1.rzgSow&previewAs=guest)
32. [阿里云大学-Python完全自学手册](https://edu.aliyun.com/course/505?spm=5176.10731491.0.0.JkZ1Yx)
33. [阿里云大学-PHP完全自学手册](https://edu.aliyun.com/course/509?spm=5176.10731491.0.0.JkZ1Yx)
34. [阿里云大学-Redis数据库入门](https://edu.aliyun.com/course/22?spm=5176.10731491.0.0.loO18T)
35. [阿里云大学-Linux完全自学手册](https://edu.aliyun.com/course/504?spm=5176.8764728.aliyun-edu-course-tab.1.J8UxM0&previewAs=guest)
36. [阿里云大学-Memcached完全自学手册](https://edu.aliyun.com/course/475?spm=5176.8764728.course-475.1.sOLlpc)
37. [阿里云大学-SQL完全自学手册](https://edu.aliyun.com/course/467?spm=5176.8764728.course-467.1.ncQ2Wr)
38. [阿里云大学-MySQL完全自学手册](https://edu.aliyun.com/course/479?spm=5176.8764728.course-479.1.tcQrp2)
39. [阿里云大学-MongoDB完全自学手册](https://edu.aliyun.com/course/478?spm=5176.8764728.course-478.1.vzPZ4G)
40. [阿里云大学-分布式数据库技术与实现](https://edu.aliyun.com/course/37?spm=5176.8764728.aliyun-edu-course-tab.1.I3p7EQ&previewAs=guest)
41. [swoole 微课堂](https://course.swoole-cloud.com/)
42. [AlgoCasts](https://algocasts.io/) - 算法学习网站【付费】。
43. [百面网](http://www.100mian.com/) - IT 公司面试神器。
44. [我要自学网](https://www.51zxw.net/) - 多内容学习网站。
45. [力扣](https://leetcode-cn.com/) - 国内知名算法学习网站，大量的面试题目。
46. [菜鸟教程](https://www.runoob.com/) - 提供了编程的基础技术教程和在线代码运行。
47. [codecademy](http://codecademy.cn/) - 在线互动编程学习网站。
48. [coursera](https://www.coursera.org/) - 大型公开在线课程项目。
49. [stack overflow](https://stackoverflow.com/) - 国外程序员问答平台。
50. [良许 Linux](http://www.liangxu.co/) - 良许个人网站，技术分享、学习资料共享。
51. [Algorithm Visualizer](https://algorithm-visualizer.org/) 著名的算法可视化工具。
52. [手摸手带你创建 php 现代化框架](https://www.kancloud.cn/learnku_/framework/1834349)【书】
53. [EDX](https://www.edx.org/learn/chinese) - 由麻省理工和哈佛大学于2012年4月联手创建的大规模开放在线课堂平台。
54. [学堂在线](https://www.xuetangx.com/) - 学堂在线是清华大学于2013年10月发起建立的慕课平台。
55. [coursera](https://www.coursera.org/) - 大型公开在线课程项目[美]。
56. [优达学城](https://cn.udacity.com/) - 自学编程网站。
57. [多邻国](http://wwww.duolingo.cn) - 最佳的外语学习方法，在 100 多个国家可使用。
58. [中国大学 MOOC](https://www.icourse163.org) - 国内优质的中文 MOOC 学习平台。
59. [牛客网](https://www.nowcoder.com/) - 在线编程、编程学习、笔试面试。



## 51~100

1. [美团技术团队](https://tech.meituan.com/) - 技术文章。
2. [Bash 脚本教程](https://wangdoc.com/bash/) - 本教程介绍 Linux 命令行 Bash 的基本用法和脚本编程。【阮一峰 著】
3. [SSH 教程](https://wangdoc.com/ssh/) - 本教程介绍 SSH（主要是它的实现 OpenSSH）的概念和基本用法，也可以当作手册查询。【阮一峰 著】
4. [Web API 教程](https://wangdoc.com/webapi/) - Web API 教程，提供各种浏览器 API 文档。【阮一峰 著】
5. [ECMAScript 6 教程](https://wangdoc.com/es6/) - 是一本开源的 JavaScript 语言教程，全面介绍 ECMAScript 6 新引入的语法特性。【阮一峰 荐】
6. [JavaScript 教程](https://wangdoc.com/javascript/) - 本教程全面介绍 JavaScript 核心语法，覆盖了 ES5 和 DOM 规范的所有内容。【阮一峰 著】
7. [HTML 教程](https://wangdoc.com/html/) - HTML 语言是互联网开发的基础。【阮一峰 著】
8. [Markdown 教程](markdown/) 【菜鸟教程】
9. [华为开发者学院](https://developer.huawei.com/consumer/cn/training/) - 面向开发者学习、认证和职业发展的人才生态平台。
10. [技术胖](http://www.jspang.com/) - 术胖-胜洪宇关注 web 前端技术-前端免费视频第一博客。
11. [极客时间](https://time.geekbang.org/) - 国内技术视频学习网站。
12. [操作系统实战45讲](https://time.geekbang.org/column/intro/100078401) - 彭东（网名 LMOS，Intel 傲腾项目关键开发者）
13. [书栈网](https://www.bookstack.cn/)
15. [W3Cschool](https://www.w3cschool.cn/) - 编程狮-推荐。
16. [Chrome 浏览器客户端调试大全](http://www.igeekbar.com/igeekbar/post/156.htm)
17. [programiz](https://www.programiz.com) - 免费学习编程【英文】，学习的语言：python、C、java、JavaScript、C++、DS 和算法、kotlin、swift。
18. [网道](https://wangdoc.com/) - 一个互联网开发文档的网站。【推荐】
19. [visualgo](https://visualgo.net/zh) - 数据结构和算法动态可视化。
20. [Docker 入门教程](https://github.com/docker/getting-started) - 官方开源教程文档。
21. [Rust 入门教程](https://docs.microsoft.com/zh-cn/learn/paths/rust-first-steps/) - 微软开源 Rust 入门教程。
22. 中山大学操作系统项目 - [github](https://github.com/YatSenOS/YatSenOS-Tutorial-Volume-1) [gitee](https://gitee.com/nelsoncheung/sysu-2021-spring-operating-system)
23. [前端内参](https://github.com/coffe1891/frontend-hard-mode-interview/ ) - 该书共有 11 章，覆盖了技术面试、JavaScript 特性解析、数据结构与算法、主流框架、开发工具、编程范式、设计原则与编程范式等内容。[官网](https://coffe1891.gitbook.io/frontend-hard-mode-interview/)
24. [Analysis Tools](https://analysis-tools.dev/) - 实用开发工具网站。
25. [用聪明的方法学习 Vim](https://github.com/wsdjeg/Learn-Vim_zh_cn) - 该书将从零开始，着重于 Vim 编辑器的重点功能，教你如何快速上手并使用 Vim。
26. [regexlearn](https://regexlearn.com/) - 从 0 到 1：学习正则表达式 [github](https://github.com/aykutkardas/regexlearn.com)
27. [计算机教育中缺失的一课](https://missing-semester-cn.github.io/) - 该课程主要讲授命令行、文本编辑器、Git 版本控制系统等工具的使用，以及关于元编程、安全和密码学等知识的科普。
28. [Linux 内核模块编程指南](https://sysprog21.github.io/lkmpg/) - 主要讲解 Linux 内核模块简介、模块交互、系统调用、阻塞进程和线程、调度任务、中断处理程序等内容。 [github](https://github.com/sysprog21/lkmpg)
29. [Effective Modern C++](https://github.com/kelthuzadx/EffectiveModernCppChinese) - 作为高效 C++ 丛书之一，通过对复杂技术知识点的清晰阐释，让其成为 C++ 程序设计指南业界标杆。
30. [Road To Coding](https://r2coding.com) - 自学编程以来所用资源和分享内容的大聚合。
31. [开放原子教育](https://gitee.com/openatom-university) - 中国开放原子基金教育学习课件。
32. [JavaScript 30 天编程挑战](https://github.com/Asabeneh/30-Days-Of-JavaScript) - 内容主要包括前端开发环境配置、VSCode 代码编辑器使用介绍、JavaScript 语法基础、JSON 处理、DOM 对象操作、数据可视化项目编写等。
33. [Programiz](https://www.programiz.com/) - 针对初学者代码学习和示例代码，支持众多编程语言。【英文】
34. [w3schools](https://www.w3schools.com/) - 拥有世界上最大的网站开发者网站。【英文】
35. [r2coding](https://r2coding.com/) - 自学编程之路。
36. [free-programming-books-zh_CN](https://github.com/justjavac/free-programming-books-zh_CN) - 免费的编程中文书籍索引。
37. [计算机教育中缺失的一课](https://missing-semester-cn.github.io/) - 讲授命令行、强大的文本编辑器的使用、使用版本控制系统提供的多种特性等等。
37. [计算机自学指南](https://github.com/PKUFlyingPig/cs-self-learning) - 北大学霸开源。【推荐】
37. [小林 x 图解计算机基础](https://github.com/xiaolincoder/CS-Base)
40. [IT项目网](https://www.itprojects.cn/) - 简单有趣好玩的编程项目，主要涵盖 python、C、C++项目。[B站-王铭东](https://space.bilibili.com/457643342)



# 工业设计

- [Rhino & keyshot 工业设计](https://www.51zxw.net/list.aspx?cid=697) - 初级【我要自学网】
- [Rhino(犀牛) 视频教程 - 初级](https://www.51zxw.net/list.aspx?cid=503) - 初级【我要自学网】


# 学科

1. [微软数学求解器](https://mathsolver.microsoft.com/zh) - 覆盖了从小学到大学的数据题解法。

