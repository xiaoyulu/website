# stable-audio-tools

- https://github.com/Stability-AI/stable-audio-tools

## 环境变量

HF_ENDPOINT 是 Hugging Face 相关库官方支持的一个环境变量，它允许用户通过指定一个镜像站点的主机名来下载和上传模型、数据集，而无需修改原生的 transformers 代码。

**功能**

- 允许用户通过镜像站点（如 hf-mirror.com）访问 Hugging Face 的资源，提供稳定、快速的下载和上传服务。
- 解决因网络问题或 Hugging Face 官方服务器负载过高导致的下载和上传困难。

**设置方法**

- **Linux/Mac OS**：在终端中执行 `export HF_ENDPOINT="https://hf-mirror.com"`，然后可以将此命令写入到 `~/.bashrc`（Linux）或`~/.zshrc`（Mac OS）文件中，以便每次打开终端时自动加载。
- **Windows**：在 Powershell 中执行 `$env:HF_ENDPOINT="https://hf-mirror.com"`。
- **Python**：在 Python 脚本中，可以使用 `import os` 和 `os.environ['HF_ENDPOINT'] = 'https://hf-mirror.com'` 来设置环境变量，但需要注意，这一行代码需要在导入 Hugging Face 相关库之前执行。



## 局域网访问

```python
# run_gradio.py
def main(args):
    interface.queue()
    interface.launch(share=True, server_name='0.0.0.0', server_port=7860, auth=(args.username, args.password) if args.username is not None else None)
```



# 问题

参考文档：

- https://zhuanlan.zhihu.com/p/694710729
- https://blog.csdn.net/David_jiahuan/article/details/139524496

```
OSError: Can't load tokenizer for 'bert-base-uncased'. If you were trying to load it from 'https://huggingface.co/models', make sure you don't have a local directory with the same name. Otherwise, make sure 'bert-base-uncased' is the correct path to a directory containing all relevant files for a BertTokenizer tokenizer.
```

解决方案就是：如果你翻墙或者停掉代理都不能解决，下面的方法亲测有效：

```shell
$ HF_ENDPOINT=https://hf-mirror.com python运行命令
```

或者直接改源码

```python
# run_gradio.py
import os
os.environ['HF_ENDPOINT'] = 'https://hf-mirror.com'

from stable_audio_tools import get_pretrained_model
from stable_audio_tools.interface.gradio import create_ui
import json
import torch
```

