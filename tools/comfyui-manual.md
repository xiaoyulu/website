# ComfyUI 使用手册

视频教程

- [零基础学会ComfyUI工作流出图](https://www.bilibili.com/video/BV147421R7RX/?vd_source=69eb0512c2a2b3678dfe289e0a8a4d4f)
- [ComfyUI新手指南](https://space.bilibili.com/1814756990/channel/collectiondetail?sid=2187799)
- [ComfyUI 教程](https://www.bilibili.com/video/BV1Fr42157AF/?spm_id_from=333.999.0.0&vd_source=69eb0512c2a2b3678dfe289e0a8a4d4f)

## 控制面板结构

### K 采样器

- 模型
- 正面条件 - CLIP 文本编码器
- 负面条件 - CLIP 文本编码器
- Latent - 【宽度】和【高度】设置图片尺寸，【批次大小】设置一次生成图片的张数



在 ComfyUI 的 K 采样器中，正面条件和负面条件是用于影响图像生成过程的重要参数。以下是关于如何在 K 采样器中使用正面条件和负面条件的详细说明：

一、正面条件（Positive Prompts）

正面条件，也称为正向提示词，是用于描述你希望生成的图像的关键特征或概念的文本输入。这些条件告诉模型你想要在图像中看到的元素、风格或主题。

使用方式：

- 在 ComfyUI 的界面中，找到用于输入正向提示词的文本框。
- 输入你的正面条件，例如 "1girl, Best quality, masterpiece, ultra high res, (photorealistic:1.4)" 等。
- 这些提示词应该简洁明了，直接描述你想要的图像特征。

影响：

- 正面条件将直接影响生成的图像的内容和风格。模型会尝试根据这些条件来生成符合要求的图像。

二、负面条件（Negative Prompts）

负面条件，也称为反向提示词，用于描述你不希望出现在生成的图像中的元素或特征。这些条件帮助模型避免生成不符合要求的图像。

使用方式：

- 同样在 ComfyUI 的界面中，找到用于输入反向提示词的文本框。
- 输入你的负面条件，例如 "use ng_deepnegative_v1_75t and badhandv4, (deepnegative)" 等。
- 这些提示词应该明确指出你不希望图像中包含的元素或特征。

影响：

- 负面条件将帮助模型在生成图像时避免某些内容或风格。这可以进一步提高生成的图像与你的期望的匹配度。

**注意事项**：

- 正面条件和负面条件的使用需要一定的技巧和经验。不同的提示词组合可能会产生不同的效果。
- 过度使用负面条件可能会导致生成的图像质量下降或不符合预期。因此，在使用时需要谨慎权衡。
- 在调整正面条件和负面条件时，可以结合使用其他参数（如 CFG、采样步数等）来进一步优化生成的图像质量。



### K 采样器属性介绍

#### 属性-随机种（Seed）

- 定义：随机种子值用于控制潜在图像的初始噪声，从而影响最终图像的合成。
- 作用：不同的 Seed 值会生成不同的图像，因此可以通过调整 Seed 值来尝试生成不同的效果。
- 数值范围：Seed 值通常是一个整数，没有固定的数值范围，但不同的整数会对应不同的初始噪声状态。

#### 属性-运行后操作（Control After Generate）

- 定义：决定种子在采样过程中的变化方式。

选项：

- Fixed：种子在采样过程中保持不变，生成的图像将具有一致的风格。
- Random：种子在每个采样步骤中随机生成，生成的图像将具有变化的风格。
- 另外可能还包括增加或减少种子的值等选项。

#### 属性-步数（Steps）

- 定义：采样步长数，即循环次数。
- 作用：采样步长越大，图像的细节越丰富，但也越容易出现噪点。
- 推荐值：对于较小的图像或较简单的图像，可以使用较小的步长数（如 15～40 步）。对于较大的图像或较复杂的图像，可以使用较大的步长数（如 20, 25, 30 等步数）。

#### 属性-CFG

- 定义：重绘幅度和提示词相关性。
- 作用：CFG 值越高，提示词的相关性越高，图像与提示词的相关性越强。
- 推荐值：通常在 0.5 到 1.0 之间，但可以根据需要进行调整。CFG 越高，饱和度越高，但不影响出图时间。

#### 属性-采样器（Sampler）

- 定义：采样算法的选择。

选项：

- Euler：经典之选，快速且可靠。
- DPMPP 2m：速度与质量的完美结合。
- Uni_PC：效率高，5-10 步快速出图。
- DPMPP_2m_SDE：质量更好。
- 还有其他多种采样器可供选择，每种都有其特点和适用场景。

#### 属性-调度器（Scheduler）

- 定义：控制降噪的方式。
选项：
- Normal：线性降噪。
- Karras：S 型降噪，推荐选择。
- Exponential：指数型降噪。

推荐：通常选择 Normal 或 Karras。

#### 属性-降噪（Denoising）

- 定义：降噪的程度。
- 作用：与步数有关，通常设置为 1，表示完全按照上方输入的步数进行处理。
- 数值范围：通常设置为 1，但也可以根据需要进行调整。例如，设置为 0.1 表示只进行 10% 的降噪处理。

通过合理设置这些属性，用户可以在 ComfyUI 中实现对图像生成过程的精确控制，从而生成出符合需求的图像。





### 生成图片

点击【执行队列】生成图片。



## 提示词

目前只支持英文，无法识别中文。

以下是【正面条件】提示词的示例：

-  a female character with long, flowing hair that appears to be made of ethereal, swirling patterns resembling the Northern Lights or Aurora Borealis. The background is dominated by deep blues and purples, creating a mysterious and dramatic atmosphere. The character's face is serene, with pale skin and striking features. She wears a dark-colored outfit with subtle patterns. The overall style of the artwork is reminiscent of fantasy or supernatural genres
-  Digital art, portrait of an anthropomorphic roaring Tiger warrior with full armor, close up in the middle of a battle, behind him there is a banner with the text "Open Source".
-  photo of a dog and a cat both standing on a red box, with a blue ball in the middle with a parrot standing on top of the ball. The box has the text "SD3"
-  selfie photo of a wizard with long beard and purple robes, he is apparently in the middle of Tokyo. Probably taken from a phone.
-  A vibrant street wall covered in colorful graffiti, the centerpiece spells "SD3 MEDIUM", in a storm of colors
-  photo of a young woman with long, wavy brown hair tied in a bun and glasses. She has a fair complexion and is wearing subtle makeup, emphasizing her eyes and lips. She is dressed in a black top. The background appears to be an urban setting with a building facade, and the sunlight casts a warm glow on her face.
-  anime art of a steampunk inventor in their workshop, surrounded by gears, gadgets, and steam. He is holding a blue potion and a red potion, one in each hand
-  photo of picturesque scene of a road surrounded by lush green trees and shrubs. The road is wide and smooth, leading into the distance. On the right side of the road, there's a blue sports car parked with the license plate spelling "SD32B". The sky above is partly cloudy, suggesting a pleasant day. The trees have a mix of green and brown foliage. There are no people visible in the image. The overall composition is balanced, with the car serving as a focal point.
-  photo of young man in a black suit, white shirt, and black tie. He has a neatly styled haircut and is looking directly at the camera with a neutral expression. The background consists of a textured wall with horizontal lines. The photograph is in black and white, emphasizing contrasts and shadows. The man appears to be in his late twenties or early thirties, with fair skin and short, dark hair.
-  photo of a woman on the beach, shot from above. She is facing the sea, while wearing a white dress. She has long blonde hair






## 快捷方式

- Ctrl + Enter：将当前图形作为第一个进行生成排队
- Ctrl + Shift + Enter：将当前图形作为第一个进行生成排队
- Ctrl + Z/Ctrl + Y：复原/重做
- Ctrl + S：保存工作流
- Ctrl + O：加载工作流
- Ctrl + A：选择所有节点
- Alt + C：折叠/取消折叠所选节点
- Ctrl + M：使选定节点静音/取消静音
- Ctrl + B：绕过选定的节点（就像节点从图形中删除，导线通过重新连接一样）
- Delete/Backspace：删除所选节点
- Ctrl + Backspace：删除当前图形
- Space：按住并移动光标时移动画布
- Ctrl/Shift + Click：将单击的节点添加到选择
- Ctrl + C/Ctrl + V：复制和粘贴选定节点（不维护与未选定节点输出的连接）
- Ctrl + C/Ctrl + Shift + V：复制和粘贴选定节点（保持从未选定节点的输出到粘贴节点的输入的连接）
- Shift + Drag：同时移动多个选定节点
- Ctrl + D：加载默认图形
- Alt + `+`：画布放大
- Alt + `-`：画布缩小
- Ctrl + Shift + LMB + Vertical drag：画布放大/缩小
- Q：切换队列的可见性
- H：切换历史的可见性
- R：刷新图形
- Double-Click LMB：打开节点快速搜索调色板




