# ComfyUI

[ComfyUI 使用手册](stable-diffusion-3-medium-manual.md)

共享工作流

- [OpenArt AI](https://openart.ai/workflows/)
- https://comfyworkflows.com/
- [ComfyUI-Workflows-ZHO](https://github.com/ZHO-ZHO-ZHO/ComfyUI-Workflows-ZHO) - 某位 GitHub 用户的工作流合集。

## 大模型下载

- https://huggingface.co/ - 国内无法访问
- https://hf-mirror.com/ - huggingface 镜像网站
- https://civitai.com/ - 可以下载 Lora。



## checkpoints

目录：models/checkpoints

- [stable-diffusion-3-medium](https://huggingface.co/stabilityai/stable-diffusion-3-medium) - 文生图。
- [wd-illusion-fp16.safetensors](https://huggingface.co/waifu-diffusion/wd-1-5-beta3) - 美学模型，生成漫画方面的图片。
- [AbyssOrangeMix2_hard.safetensors](https://huggingface.co/WarriorMama777/OrangeMixs)
- [cardosAnime_v10.safetensors](https://huggingface.co/yz54321/cardosAnime)
- [cardosAnime_v20.safetensors](https://huggingface.co/nergaldarski/CardosAnimeV2.0)
- [sd_xl_base_1.0.safetensors](https://huggingface.co/stabilityai/stable-diffusion-xl-base-1.0)
- [sd_xl_turbo_1.0_fp16.safetensors](https://huggingface.co/stabilityai/sdxl-turbo)
- [svd_xt.safetensors](https://huggingface.co/stabilityai/stable-video-diffusion-img2vid-xt)
- [diffusion_pytorch_model.safetensors](https://huggingface.co/lllyasviel/sd-controlnet-canny)
- [cosxl_edit.safetensors](https://huggingface.co/stabilityai/cosxl)



## upscale_models

目录：models/upscale_models

- [upscale_models](https://huggingface.co/tomjackson2023/upscale_models) - 众多图片放大大模型。



## loras

目录：models/loras

- [Loras](https://huggingface.co/datasets/mpiquero/Loras) - 众多的 loras。



## vae

目录：models/vae

- [vae-fft-mse-840000-era-pruned.safetensors](https://huggingface.co/stabilityai/sd-vae-ft-mse-original)



参考文档：https://www.freedidi.com/12706.html

- sd3_medium.safetensors 4.34GB - 基础大模型【CPU 版】
- sd3_medium_incl_clips.safetensors 5.97GB - 训练数据
- sd3_medium_incl_clips_t5xxlfp16.safetensors 15.8GB - 基础大模型【GPU 高清版】
- sd3_medium_incl_clips_t5xxlfp8.safetensors 10.9GB - 基础大模型【GPU 标准版】

按照自己的电脑配置选择你需要的基础大模型。

需要安装 [ComfyUI](https://github.com/comfyanonymous/ComfyUI) 控制面板才可以使用，将基础大模型放在：

```
models/checkpoints/
```

安装 ComfyUI 中文语言包 [AIGODLIKE-ComfyUI-Translation](https://github.com/AIGODLIKE/AIGODLIKE-ComfyUI-Translation)，然后在 ComfyUI 设置中心切换语言为中文。

```shell
$ cd ComfyUI/custom_nodes
$ git clone https://github.com/AIGODLIKE/AIGODLIKE-COMFYUI-TRANSLATION.git
```

运行 comfyui 控制面板，通过添加参数指定 IP 地址与端口，如果 `--listen` 未指定，本局域网其它 IP 会无法访问 comfyui 控制面板。

```shell
$ python main.py --listen='0.0.0.0' --port=8188
```



## custom_nodes

[custom节点fyUI Nodes Info](https://ltdrdata.github.io/) - 节点使用方式说明。

节点放置在 custom_nodes 目录中。

- [ComfyUI-Manager](https://github.com/ltdrdata/ComfyUI-Manager) - 是一个扩展，旨在增强 ComfyUI 的可用性。它提供了安装、删除、禁用和启用 ComfyUI 的各种自定义节点的管理功能。
- [ComfyUI_experiments](https://github.com/comfyanonymous/ComfyUI_experiments) - 一些实验性的自定义节点。
- [ComfyUI-Inspire-Pack](https://github.com/ltdrdata/ComfyUI-Inspire-Pack) - 此存储库为 ComfyUI 提供了各种扩展节点。
- [ComfyUI-Impact-Pack](https://github.com/ltdrdata/ComfyUI-Impact-Pack) - ComfyUI 的自定义节点包此自定义节点有助于通过 Detector、Detailer、Upscaler、Pipe 等方便地增强图像。
- [ComfyUI-extension-tutorials](https://github.com/ltdrdata/ComfyUI-extension-tutorials) - ComfyUI 扩展教程。
- [ComfyUI-WD14-Tagger](https://github.com/pythongosssss/ComfyUI-WD14-Tagger) - ComfyUI 扩展允许从图像中查询 booru 标签。提示词反推
- [ComfyUI-Custom-Scripts](https://github.com/pythongosssss/ComfyUI-Custom-Scripts) - 自定义脚本包（含提示词补全）。
- [efficiency-nodes-comfyui](https://github.com/jags111/efficiency-nodes-comfyui) - ComfyUI 效率节点包。
- [OneButtonPrompt](https://github.com/AIrjen/OneButtonPrompt) - 简易负面词。
- [ComfyUI_Comfyroll_CustomNodes](github.com/Suzie1/ComfyUI_Comfyroll_CustomNodes) - CR 节点包。
- [was-node-suite-comfyui](https://github.com/WASasquatch/was-node-suite-comfyui) - WAS 节点套件。
- [rgthree-comfy](https://github.com/rgthree/rgthree-comfy) - RG3 节点包。
- [ComfyUI-AnimateDiff-Evolved](https://github.com/Kosinkadink/ComfyUI-AnimateDiff-Evolved) - 改进的 AnimateDiff 用于 ComfyUI 和高级采样支持。
- [IP-Adapter](https://github.com/tencent-ailab/IP-Adapter/) - 图像提示适配器被设计为使预训练的文本到图像扩散模型能够生成具有图像提示的图像。
- [ComfyUI_IPAdapter_plus](https://github.com/cubiq/ComfyUI_IPAdapter_plus) - IPAdapter 模型的 ComfyUI 参考实现。
- [comfyui-reactor-node](https://github.com/Gourieff/comfyui-reactor-node) - ComfyUI 快速简单的换脸扩展节点。
- [ComfyUI-Catcat](https://github.com/jw782cn/ComfyUI-Catcat) - ComfyUI-Catcat 是一个扩展，它将等待时间转化为一个有趣的场景，通过随机的猫 GIF 为 ComfyUI 渲染注入欢乐，使每个加载时刻都成为意想不到的喜悦。
- [masquerade-nodes-comfyui](https://github.com/BadCafeCode/masquerade-nodes-comfyui) - ComfyUI 的一组强大的掩码相关节点。
- [ComfyUI-Easy-Use](https://github.com/yolain/ComfyUI-Easy-Use) - 是一个化繁为简的节点整合包, 在 [tinyterraNodes](https://github.com/TinyTerra/ComfyUI_tinyterraNodes) 的基础上进行延展，并针对了诸多主流的节点包做了整合与优化，以达到更快更方便使用ComfyUI的目的，在保证自由度的同时还原了本属于Stable Diffusion的极致畅快出图体验。
- [comfyui_controlnet_aux](https://github.com/Fannovel16/comfyui_controlnet_aux) - ComfyUI 的 ControlNet 辅助预处理器。
- [ComfyUI_Custom_Nodes_AlekPet](https://github.com/AlekPet/ComfyUI_Custom_Nodes_AlekPet) - 扩展 Comfyui 功能的自定义节点。里面有翻译文本的功能。
- [comfyui-portrait-master](https://github.com/florestefano1975/comfyui-portrait-master) - 肖像大师。
- [comfyui-portrait-master-zh-cn](https://github.com/ZHO-ZHO-ZHO/comfyui-portrait-master-zh-cn) - 肖像大师中文版【推荐】。
- [ComfyUI-DiffSynthWrapper](https://github.com/kijai/ComfyUI-DiffSynthWrapper) - 128 帧视频生成模型。
- [ComfyUI-KwaiKolorsWrapper](https://github.com/kijai/ComfyUI-KwaiKolorsWrapper) - 用于 Kwai-Kolors 的 ComfyUI 包装器。



## clip

- [text_encoders/clip_g.safetensors](https://huggingface.co/stabilityai/stable-diffusion-3-medium)
- [text_encoders/clip_l.safetensors](https://huggingface.co/stabilityai/stable-diffusion-3-medium)
- [text_encoders/t5xxl_fp16.safetensors](https://huggingface.co/stabilityai/stable-diffusion-3-medium)



