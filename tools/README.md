# 工具

1. [阿里云开源镜像](https://developer.aliyun.com/mirror/)
2. [宝塔](http://www.bt.cn/) - 服务器运维面板
3. [百度开发者搜索](https://kaifa.baidu.com/)
4. [漏洞检测工具大全](http://www.oschina.net/project/tag/361/loophole-tools)
5. [VirtualBox](https://www.virtualbox.org) - 虚拟机【免费】
6. AI 编程助手
   - Huawei Cloud CodeArts Snap - 华为云 CodeArts Snap 智能编程助手。
     - Huawei Cloud CodeArts Check - 华为云 CodeArts Check 代码检查插件。
   - DevPilot - 众安保险 AI 代码助手。
   - [TONGYI Lingma](https://tongyi.aliyun.com/lingma) - 通义灵码，是一款基于通义大模型的智能编码辅助工具。
   - [CodeGeex](https://codegeex.cn/activity) - 北京智谱华章科技有限公司。
   - [Fitten Code](https://code.fittentech.com/) - 北京非十科技有限公司。
   - Baidu Comate - 百度的文心快码。
   - [MarsCode](https://www.marscode.cn/) - 豆包 AI 编程助手。
   - [Tencent Cloud AI Code Assistant](https://plugins.jetbrains.com/plugin/24379-tencent-cloud-ai-code-assistant) - 腾讯云 AI 代码助手。
   - [CodeGPT](https://plugins.jetbrains.com/plugin/21056-codegpt) - AI 代码助手，可接入第三方大模型。
   - [continue](https://plugins.jetbrains.com/plugin/22707-continue) - AI 代码助手，可接入第三方大模型。
7. JetBrains 系列插件。主题文件： `GitHub.icls`, `google-developer-tibau_0.icls`。[PHPstorm 设置优化](https://blog.csdn.net/weixin_28709715/article/details/114874699)
     插件
   - CodeGlance Pro - 代码地图
   - Translation - 翻译
   - Rainbow Brackets - 成对括号着色
   - Grep Console - 控制窗打印日志颜色区分
   - Statistic - 全局代码统计信息
   - Markdown Navigator - Markdown 编辑器
   - RestfulToolkit - 让 postman 下岗的插件
   - GsonFormat - json 格式数据 -> 对象的快速代码自动生成插件
   - MyBatis Log Plugin - Mybatis 的 xml 文件中的 SQL 语句都是拼装起来的
   - Lombok - java 库插件
   - [Reloadium](https://reloadium.io/) Python 热重载调试工具，vscode 也适用。
   - [Database Navigator](https://plugins.jetbrains.com/plugin/1800-database-navigator) - 数据库开发、脚本编写和导航工具。
8. [MobaXterm](https://mobaxterm.mobatek.net/) - 全能终端，增强型远程连接工具。
   - 下载选择 MobaXterm Home Edition v*  (Portable edition) 版的，因为不需要安装，是绿色版。
9. [PostMan](https://www.getpostman.com/) - 接口测试工具。
10. [ApiPost](https://www.apipost.cn/) - API 文档、调试、Mock、测试一体化协作平台。
11. [ApiFox](https://www.apifox.cn/) - API 文档、API 调试、API Mock、API 自动化测试。
12. 数据抓包工具。
    - [Wireshark](https://www.wireshark.org/download.html)
    - [Fiddler](https://www.telerik.com/fiddler) - 是白帽子黑客最经常使用的，是可以进行 http 协议调试的一种 web 报文渗透工具。[建议用破解版]
    - [Burp Suite](https://portswigger.net) - 是用于攻击 web 应用程序的集成平台，包含了许多工具。
13. [HBuilder](https://www.dcloud.io/) - 前端开发混合工具。
14. [HeidiSQL](https://www.heidisql.com/) - MySQL 工具。
15. [ossbrowser](https://www.alibabacloud.com/help/zh/doc-detail/61872.htm) - 阿里云官方提供的 OSS 图形化管理工具。
16. [华为浏览器](https://consumer.huawei.com/cn/mobileservices/browser/) 华为出品，必属精品。
17. [linkedin](https://github.com/linkedin/school-of-sre) - LinkedIn 在 GitHub 上开源了其企业内部的 SRE 技术课程，主要包含 Linux、Git、Python、Web、MySQL、大数据、系统设计、网络安全等内容。
18. [typora](https://www.typora.io/) markdown 编辑器。
19. [MarkText](https://github.com/marktext/marktext) markdown 编辑器。[官网](https://www.marktext.cc/)
20. [photo forensics](http://fotoforensics.com/) - 图片 PS 痕迹鉴定。
21. [draw.io](https://www.diagrams.net/) - draw.io 是一款免费的在线图表编辑工具, 可以用来编辑工作流, BPM, org charts, UML, ER 图, 网络拓朴图等。[gitee](https://gitee.com/mirrors/drawio) [github](https://github.com/jgraph/drawio-desktop)
22. [lionsoul/ip2region](https://gitee.com/lionsoul/ip2region) - 准确率 99.9% 的离线IP地址定位库。
23. [rustdesk](http://rustdesk.com/zh/) - 开源远程桌面软件。
24. [Nushell](https://www.nushell.sh/) - shell 工具。
25. [Ventoy](https://www.ventoy.net) - 新一代多系统启动 U 盘解决方案【国产-装系统】[使用教程](/ventoy使用教程.md)
26. [水泽-信息收集自动化工具](https://github.com/0x727/ShuiZe_0x727) - 信息收集自动化工具(python)
27. dev-sidecar - 开源的开发者辅助工具，提供 GitHub、Stack Overflow、NPM 等开发者工具的访问加速功能。[gitee](https://gitee.com/docmirror/dev-sidecar), [github](https://github.com/docmirror/dev-sidecar)
28. [Online_Tools](https://github.com/r0eXpeR/Online_Tools) - 在线情报搜集工具。包含企业信息、IP 信息及域名备案查询工具、Web 应用指纹识别、威胁情报平台、在线提权辅助等各类工具。
29. [程序员工具](https://tool.p2hp.com/) - 程序员在线开发工具。
30. [visualgo](https://visualgo.net/zh) - 数据结构和算法动态可视化。
31. [python tutor](https://pythontutor.com/) - 代码可视化网站。支持多种编程语言。
32. [iroiro](https://github.com/antfu/iroiro) - 色彩查询工具。
33. [penpot](https://penpot.app/) - 开源的 UI 设计与原型制作平台。[github](https://github.com/penpot/penpot)
34. [libreofficeg](https://zh-cn.libreoffice.org/) - 免费开源的 office 办公软件。
35. [vnote](https://github.com/vnotex/vnote) - markdown 编辑器。
36. [codeup](https://codeup.aliyun.com/) - 阿里云出品的代码托管平台
37. [工蜂](https://git.code.tencent.com/) - 腾讯出品的代码托管平台
38. [码云](https://gitee.com/) - 开源中国出品的代码托管平台
39. [Redis 桌面管理客户端](https://gitee.com/qishibo/AnotherRedisDesktopManager) - 兼容 Windows、Mac、Linux 等主流系统。性能出众，轻松加载海量键值。
40. [Tiny RDM](https://redis.tinycraft.cc/) - Redis 桌面管理客户端。
41. [ajihuo](https://www.ajihuo.com/) - jetbrains 系列激活工具。
42. [小白学堂](http://www.itmind.net/) - 付费软件破解激活网站。
43. [MasterGo](https://mastergo.com/) - 协同产品设计工具，蓝湖出品。
44. [chrome linux 版](https://www.google.cn/intl/zh-CN/chrome/) - 下载地址。
45. [FFmpeg 文档](https://www.cnblogs.com/Finley/p/8646711.html) - 是一个用于音视频处理的自由软件，被广泛用于音视频开发。[官网](https://www.ffmpeg.org/)
46. [LibreOffice](https://zh-cn.libreoffice.org/) - office 办公软件，支持众多平台。
47. [亿图](https://www.edrawsoft.cn/) - 亿图软件可让您轻松绘制流程图，思维导图，信息图，组织架构图，网络拓扑图，户型图，电路图等210种绘图类型。
48. [Android Debug Bridge](https://adbdownload.com/)
49. [DB Browser for SQLite](https://sqlitebrowser.org/) - SQLite 数据库浏览器工具。
50. [LOGO 神器](https://www.logosc.cn/) - LOGO 设计 AI 工具。【国产】
51. [悟空图像](https://www.photosir.com/#/PhotoSir) - 国内一款可以替代 AdobePhotoShop 的专业图像处理软件，采用全新的设计理念和人工智能算法，让每个用户都能快速上手、快速出图。悟空图像支持 50 亿像素级超大图片处理，双向兼容 PS 文件格式，更支持全平台运行。
52. [帮小忙](https://tool.browser.qq.com/) - 腾讯 QQ 浏览器在线工具箱。
53. [LM Studio](https://lmstudio.ai/) - 本地大语言模型（LLM）平台，致力于简化 LLM 的使用和管理。它支持离线运行模型，并提供直观的界面，方便用户进行文本生成、模型微调和文档交互。[github](https://github.com/lmstudio-ai)
54. [Ollama](https://ollama.com/) - 一款开源的大型语言模型服务工具。 
55. [Anythingllm](https://anythingllm.com/) - 是 Mintplex Labs 推出的一款开源的全栈 AI 应用程序。
56. [cherry-ai](https://cherry-ai.com/) - 是一款功能强大的多模型服务桌面客户端。
57. [OfficeAI助手](https://www.office-ai.cn/) - 是一款免费的智能AI办公工具软件，专为 Microsoft Office 和 WPS 用户打造。
58. [mermaid](http://mermaid.js.org) - 基于 JavaScript 的图表和图表工具，呈现受 Markdown 启发的文本定义以动态创建和修改图表。[在线体验](https://www.mermaidchart.com/play?utm_source=mermaid_js&utm_medium=banner_ad&utm_campaign=visual_editor)
59. [Deepseek Artifacts](https://deepseekartifacts.com/zh/deepseek) - 是由 **HuggingFace** 联合 **DeepSeek推出的 **免费开源工具**，旨在通过 AI 能力简化前端开发流程，专注于 React 和 TailwindCSS 代码生成。
60. 浏览器扩展
      - Proxy SwitchyOmega - 强大易用的浏览器代理管理插件。



# 人工智能

1. [盘古大模型](https://www.huaweicloud.com/product/pangu.html) - 华为
2. [文心一言](https://yiyan.baidu.com/) - 百度
3. [讯飞星火](https://xinghuo.xfyun.cn/) - 认知大模型。科大讯飞出品
4. [讯飞星火开源](https://gitee.com/iflytekopensource/iFlytekSpark-13B) - 认知大模型。[讯飞星火开源大模型](https://xinghuo.xfyun.cn/openSource)
5. [豆包](https://www.doubao.com/) - 字节跳动
6. [通义](https://tongyi.aliyun.com/) - 阿里云。
7. [KimiAI](https://kimi.moonshot.cn/) - Moonshot AI 出品的智能助手，大语言模型。
8. [腾讯元宝](https://yuanbao.tencent.com/chat) - 腾讯。
9. [紫东太初大模型](https://taichu-web.ia.ac.cn/) - 中国科学院自动化研究所和武汉人工智能研究院推出新一代大模型，从三模态走向全模态，支持多轮问答、文本创作、图像生成、3D理解、信号分析等全面问答任务，拥有更强的认知、理解、创作能力，带来全新互动体验。
10. [天工AI](https://www.tiangong.cn/) - 通过自然语言与用户进行问答交互，AI 生成能力可满足文案创作、知识问答、逻辑推演、数理推算、代码编程等多元化需求。
11. [gpt_academic](https://github.com/binary-husky/gpt_academic) - 中科院学术 GPT。为 GPT/GLM 等 LLM 大语言模型提供实用化交互接口，特别优化论文阅读/润色/写作体验，模块化设计，支持自定义快捷按钮、函数插件，支持 Python 和 C++ 等项目剖析、自译解功能，PDF/LaTex 论文翻译、总结功能，支持并行问询多种 LLM 模型，支持 chatglm3 等本地模型。接入通义千问, deepseekcoder, 讯飞星火, 文心一言, llama2, rwkv, claude2, moss 等。
12. [智谱AI](https://open.bigmodel.cn/) - 大模型 MaaS 开放平台。
13. Paddle(飞桨) - AI 算法模型。[gitee](https://gitee.com/paddlepaddle/Paddle) [github](https://github.com/PaddlePaddle/Paddle)
14. [AI 老照片修复](https://www.modelscope.cn/studios/iic/old_photo_restoration/summary) - 基于先进的图像去噪、人像修复增强、图像上色等模型等实现。
15. [昆虫鉴别器](https://www.modelscope.cn/studios/MuGeminorum/insecta/summary) - 支持 2037 类 (可能是目, 科, 属或种等) 昆虫或其他节肢动物。
16. [Hugging Face](https://huggingface.co/) - 是一个机器学习（ML）和数据科学平台及社区，为用户提供了丰富的资源和工具，以支持机器学习模型的构建、部署和训练。【国内无法访问】
17. [HF-Mirror](https://hf-mirror.com/) - huggingface 镜像网站
18. [CodeFuse](https://codefuse.ai) - 蚂蚁百灵研发助手。
19. [深度求索](https://www.deepseek.com/) - 提供 DeepSeek-v3 等开源大模型的使用。
