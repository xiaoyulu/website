# JavaScript

[wiki](wiki/)



1. [Vue.js 技术论坛](https://learnku.com/vuejs)
2. [Node.js 技术论坛](https://learnku.com/nodejs)
3. [JavaScript 教程](https://www.liaoxuefeng.com/wiki/1022910821149312)
4. [飞冰](https://github.com/alibaba/ice) - 阿里巴巴开源前端项目。
5. [火狐 MDN Web 文档](https://developer.mozilla.org/zh-CN/) - 前端学习国外大全。
5. [pnpm](https://www.pnpm.cn/) - 速度快、节省磁盘空间的软件包管理器。npm 替代器。【推荐】
5. [chameleon 变色龙](http://cml.didi.cn/) - 一套代码运行多端，一端所见即多端所见。[github](https://github.com/didi/chameleon)
5. [30-Days-Of-JavaScript](https://github.com/Asabeneh/30-Days-Of-JavaScript) - 30 天的 JavaScript 编程挑战是在 30 天内学习 JavaScript 编程语言的分步指南。
5. [前端内参](https://coffe1891.gitbook.io/frontend-hard-mode-interview/) - 关于前端的知识分享书籍。【书】
5. [semi design](https://semi.design/zh-CN/) - 由字节跳动抖音前端与 UED 团队设计、开发并维护，包含设计语言、React 组件、主题等开箱即用的中后台解决方案，帮助设计师与开发者打造高质量产品。
5. [logic-flow](http://logic-flow.org/) - 流程图前端框架。
12. [JavaScript 和 HTML DOM 参考手册](https://www.runoob.com/jsref/jsref-tutorial.html) - 菜鸟教程。
12. [40+ Vue3 实用工具分享  技术胖整理](40+ Vue3 实用工具分享  技术胖整理.md)
14. [Font Awesome](https://fontawesome.dashgame.com/) - icon 库。





## Vue3 Note

[vue3 学习笔记](vue3-note/)



## 使用淘宝 NPM 镜像

```shell
# 全局设置淘宝镜像
$ npm config set registry https://registry.npmmirror.com
```


## js 小技巧

### 匿名函数

传入当前对象 this 执行代码

```javascript
const app = Vue.createApp({
    data(){
        return {
            message: 'this is test',
        }
    },
    methods: {
        handleClick(){
            console.log("handleClick()");
            this.message = "change content";
            setTimeout(()=>{
                this.message = "this is test";
            }, 3000);
        }
    },
    template: `
        <div>{{message}}</div>
        <div>
            <button @click="handleClick">更新</button>
        </div>
    `,
});
const vm = app.mount("#app");

// 无参数的匿名函数
(function () {
    console.log('zxx')
})();

// 带参数的匿名函数
(function (a, b, c) {
    console.log('参数一:', a) // 参数一: 这是普通函数传参的地方
    console.log('参数二:', b) // 参数二: 我是参数二
    console.log('参数三:', c) // 参数三: zxx
})('这是普通函数传参的地方', '我是参数二', 'zxx')
```