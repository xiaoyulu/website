# ajax上传文件

html 代码

```html
<form id="uploadForm">
	<input type="file" name="file" />
</form>
```

js 代码

`FormData()` 会获取 form 表单里的所有数据。

```javascript
let uri 	 = ""
let formData = new FormData($("#uploadForm")[0])
$.ajax({
    type:"post",
    url:uri,
    data:formData,
    contentType:false,
    processData:false,
    success:function(response){
        console.log(response)
    },
    error:function(){
        console.log("error")
    }
})
```

