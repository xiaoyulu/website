# iframe 调用父页面的方法

当父页面使用 iframe 引入了子页面，想要调用父页面的方法，应该如何是好？



在父页面使用 window 定义一个全局的方法

```javascript
window.success = function(){
    console.log("window.success")
}
```



子页面直接调用

```javascript
window.success()
```



原理是 window 是系统级的全局变量，所以只要 `success()` 在父页面前定义，子页面就可以调用。



参考文档 https://blog.csdn.net/csdn_chengpeng/article/details/102959017