# Vue3 Note



## 目录结构



| 目录/文件    | 说明                      |
| ------------ | ------------------------- |
| build        | 项目构建(webpack)相关代码 |
| config       | 配置目录                  |
| node_modules | npm 加载的项目依赖模块    |
| src          | 开发目录                  |
| static       | 静态资源目录              |
| public       | 公共资源目录              |
| test         | 出事测试目录              |
| .xxx         | 配置文件                  |
| index.html   | 入口文件                  |
| package.json | 项目配置文件              |
| README.md    | 项目说明文档              |



src 包含目录及文件：

- assets: 放置一些图片，如 logo 等。
- components: 目录里面放了一个组件文件，可以不用。
- App.vue: 项目入口文件，我们也可以直接将组件写这里，而不使用 components 目录。
- main.js: 项目的核心文件。
- index.css: 样式文件。



## 创建实例

```javascript
// 创建 vue 实例
const app = Vue.createApp({});

// 模板挂在到的 dom 节点
const vm = app.mount("#app");
```



## 选项



### data

返回一个对象，以 $data 的形式存储在组件实例中。

```JavaScript
const app = Vue.createApp({
    data(){
        return {
            message: 'this is a test',
            object:{"key":"value"},
            list:[],
        }
    }
});
```



### methods

在组件中添加方法。

```javascript
const app = Vue.createApp({
    data(){
        return { count:4 }
    },
    methods: {
        increment(){
            // this 指向该组件实例
            this.count++;
        }
    }
});
```





### mounted

一个声明周期钩子函数，页面加载渲染完成，自动执行。

```javascript
Vue.createApp({
    mounted(){
        // todo
    },
}).mount("#app");
```





## 模板与数据绑定



`{{...}}` 数据绑定。

```javascript
const app = Vue.createApp({
	data(){
		return { message: 'test' }
	}
	template: `我是模板 {{message}}`,
});
```



### v-once

html 标签内容只更新一次。

```html
<span v-once>{{message}}</span>
```



### v-html

输出 html 代码。

```html
<div id="app"></div>

<script>
    const app = Vue.createApp({
        data(){
            return {
                message: 'this is a test.',
                vhtml: 'hellow world',
            }
        },
        template: `
            <p>数据绑定: {{message}}</p>
            <p>v-html: <span v-html="vhtml"></span> </p>
        `,
    });
    const vm = app.mount('#app');
</script>
```



### v-bind

html 属性定义。

```html
<div v-bind:disabled="true">test</div>
```



### 参数

参数在指令后以 冒号 指名。

```html
<a v-bind:href="url">test</a>
```



### 修饰符

修饰符是以点指明的特殊后缀，用于指出一个指令应该以特殊方式绑定。

```html
<form v-on:submit.prevent="onSubmit"></form>
```



### v-model

双向数据绑定。

```html
<div>
    <p>{{message}}</p>
	<input v-model="message" />
</div>

<script>
	const app = Vue.createApp({
        data(){
            return { message: '' }
        }
    });
    const vm = app.mount("#app");
</script>
```

**v-model** 指令用来在 input、select、textarea、checkbox、radio 等表单控件元素上创建双向数据绑定，根据表单上的值，自动更新绑定的元素的值。



### 缩写

#### v-bind

```html
<!-- 完整语法 -->
<a v-bind:href="url"></a>

<!-- 缩写 -->
<a :href="url"></a>
```



#### v-on

```html
<!-- 完整语法 -->
<a v-on:click="doSomething"></a>

<!-- 缩写 -->
<a @click="doSomething"></a>
```





## 条件语句

### v-if

表达式返回 true 时才会显示

```html
<div v-if="true">test</div>
```



### v-else

`v-if` 表达式为 false 时执行

```html
<div id="app">
    <p v-if="true">test-if</p>
    <p v-else>test-else</p>
</div>
```

v-else 、v-else-if 必须跟在 v-if 或者 v-else-if 之后。



### v-else-if

前面表达式为 false 时执行

```html
<div id="app">
    <p v-if="false">test-if</p>
    <p v-else-if="true">test-if</p>
    <p v-else>test-else</p>
</div>
```



### v-show

显示指定元素

```html
<p v-show="">test</p>
```





## 循环语句



v-for 指令需要以 **site in sites** 形式的特殊语法， sites 是源数据数组并且 site 是数组元素迭代的别名。

```html
<li v-for="site in sites">{{site.text}}</li>
```



v-for 还支持一个可选的第二个参数，参数值为当前项的索引：

```html
<li v-for="(site,index) in sites">{{index}} => {{site.text}}</li>
```



迭代整数，像 Python 一样：

```html
<li v-for="n in 10">{{n}}</li>
```





## 组件 Component

组件可以扩展 HTML 元素，封装可重用的代码。



### 注册全局组件

```vue
const app = Vue.createApp({});
app.component('my-component-name', {
	template: ``,
});
```



### 调用组件

```html
<my-component-name></my-component-name>
```



### 局部组件

```vue
// 定义组件
const widgetA = {};
const widgetB = {};
const widgetC = {};

const app = Vue.createApp({
	component:{
		'widget-a': widgetA,
		'widget-b': widgetB,
		'widget-c': widgetC,
	},
});
```



### prop

prop 是子组件用来接受父组件传递过来的数据的一个自定义属性。

父组件的数据需要通过 props 把数据传给子组件，子组件需要显式地用 props 选项声明 prop：

```vue
<site-name title="prop-test"></site-name>

const app = Vue.createApp({});
app.component('site-name', {
	props: ['title'],
	template: `<h2>{{title}}</h2>`,
});

const vm = app.mount("#app");
```



一个组件默认可以拥有任意数量的 prop，任何值都可以传递给任何 prop。





### 动态 prop

用 v-bind 动态绑定 props 的值到父组件的数据中。每当父组件的数据变化时，该变化也会传导给子组件：

```vue
<site-info v-for="site in sites" :id="site.id" :title="site.title"></site-info>



const app = Vue.createApp({
	data(){
		return {
			sites:["id":1, "title":"prop-test"]
		}
	}
});
app.component("site-info", {
	props: ["id", "title"],
	template: `<h3>{{id}} - {{title}}</h3>`,
});
const vm = app.mount("#app");
```





### prop 验证

组件可以为 props 指定验证要求。

当 prop 验证失败的时候，(开发环境构建版本的) Vue 将会产生一个控制台的警告。

type 可以是下面原生构造器：

- String
- Number
- Boolean
- Array
- Object
- Date
- Function
- Symbol

type 也可以是一个自定义构造器，使用 instanceof 检测。

```vue
const app = Vue.createApp({});
app.component('my-component', {
  props: {
    // 基础的类型检查 (`null` 和 `undefined` 会通过任何类型验证)
    propA: Number,
    // 多个可能的类型
    propB: [String, Number],
    // 必填的字符串
    propC: {
      type: String,
      required: true
    },
    // 带有默认值的数字
    propD: {
      type: Number,
      default: 100
    },
    // 带有默认值的对象
    propE: {
      type: Object,
      // 对象或数组默认值必须从一个工厂函数获取
      default: function () {
        return { message: 'hello' }
      }
    },
    // 自定义验证函数
    propF: {
      validator: function (value) {
        // 这个值必须匹配下列字符串中的一个
        return ['success', 'warning', 'danger'].indexOf(value) !== -1
      }
    }
  }
})
```





## 计算属性 computed



### computed vs methods

methods - 只要页面重新渲染，就会重新执行方法。

computed - 当计算属性依赖的内容发生变更时，才会重新执行计算。



### computed getter

获取依赖的内容时，重新执行计算。



### computed setter

设置依赖的内容时，重新执行计算。



```html
<div id="app">
    <input type="text" v-model="message">&nbsp;
    <button v-on:click="clearBtn">clear</button>&nbsp;
    <button v-on:click="getBtn">get</button>
    <p>{{message}}</p>
    <p>setter:{{setter}}</p>
    <p>getter:{{getter}}</p>
</div>

<script>
    const app = Vue.createApp({
        data(){
            return {
                message: '',
                setter: '',
                getter: '',
            }
        },
        methods:{
            clearBtn(){
                this.message = '';
            },
            getBtn(){
                this.getter = Date.now();
            }
        },
        computed: {
            // 定义 computed 的目标
            message: {
                // getter
                get: function(){
                    console.log('site.getter');
                    this.getter = this.message;
                },
                // setter
                set: function(value){
                    console.log('site.setter: ' + value);
                    this.setter = value;
                }
            }
        }
    });
    const vm = app.mount('#app');
</script>
```



色通知 vm.message 时，触发了 computed 的 `message.set()`

获取 vm.message 时，触发了 computed 的 `message.get()`





## 监听属性 watch



```html
<div id="app">
    千米: <input type="text" v-model="kilometers"><br/>
    米&nbsp;：<input type="text" v-model="meters">
</div>

<script>
    const app = Vue.createApp({
        data(){
            return {
                kilometers : 0,
                meters: 0,
            }
        },
        watch: {
            kilometers: function(value){
                this.kilometers  = value;
                this.meters = this.kilometers * 1000;
            },
            meters: function(value){
                this.kilometers = value / 1000;
                this.meters = value;
            }
        }
    });
    const vm = app.mount('#app');
</script>
```



在 input 输入框输入内容时，会触发 watch 里对应的监控函数。





## 样式绑定

`v-bind` 设置样式属性。

v-bind 的表达式可以使用字符串、对象、数组。



### class 属性绑定



### 对象语法

```html
<style>
    .redColor{color:red;}
    .test{text-align:center;}
</style>

<p v-bind:class="{'redColor': true, 'test': true}">bind</p>
```



### 数组语法

```html
<style>
    .static{width:100px;height:100px;}
    .active{background:red;}
    .text-danger{background:green;}
</style>

<div id="app">
    <div class="static" v-bind:class="[activeClass, errorClass]"></div>
</div>

<script>
    const app = Vue.createApp({
        data(){
            return {
                activeClass: 'active',
                errorClass: 'text-danger'
            }
        }
    });
    const vm = app.mount('#app');
</script>
```



### style(内联样式)

```html
<div v-bind:style="{'color': 'red'}">test</div>
```





## 事件处理



使用 `v-on` 指令来监听 DOM 事件，从而执行 javascript 代码。

`v-on` 指令可以缩写为 `@` 符号。

语法

```vue
v-on:click="methodName"

@click="methodName"
```



例如

```html
<button @click="counter += 1">click</button>
<p>{{counter}}</p>
```



### 事件修饰符



Vue.js 通过由点 **.** 表示的指令后缀来调用修饰符。



- `.stop` - 阻止冒泡
- `.prevent` - 阻止默认事件
- `.capture` - 阻止捕获
- `.self` - 只监听触发该元素的事件
- `.once` - 只触发一次
- `.left` - 左键事件
- `.right` - 右键事件
- `.middle` - 中间滚轮事件



```html
<!-- 阻止单击事件冒泡 -->
<a v-on:click.stop="doThis"></a>

<!-- 提交事件不再重载页面 -->
<form v-on:submit.prevent="onSubmit"></form>

<!-- 修饰符可以串联  -->
<a v-on:click.stop.prevent="doThat"></a>

<!-- 只有修饰符 -->
<form v-on:submit.prevent></form>

<!-- 添加事件侦听器时使用事件捕获模式 -->
<div v-on:click.capture="doThis">...</div>

<!-- 只当事件在该元素本身（而不是子元素）触发时触发回调 -->
<div v-on:click.self="doThat">...</div>

<!-- click 事件只能点击一次，2.1.4版本新增 -->
<a v-on:click.once="doThis"></a>
```



### 按键修饰符

Vue 允许 v-on 在监听键盘事件时添加按键修饰符：

```html
<!-- 只有在 keyCode 是 13 时调用 vm.submit() -->
<input v-on:keyup.13="submit">
```



全部的按键别名：

- `.enter`
- `.tab`
- `.delete` (捕获 "删除" 和 "退格" 键)
- `.esc`
- `.space`
- `.up`
- `.down`
- `.left`
- `.right`

系统修饰键：

- `.ctrl`
- `.alt`
- `.shift`
- `.meta`

鼠标按钮修饰符:

- `.left`
- `.right`
- `.middle`



实例

```html
<p><!-- Alt + C -->
<input @keyup.alt.67="clear">

<!-- Ctrl + Click -->
<div @click.ctrl="doSomething">Do something</div>
```



### `.exact` 修饰符

`.exact` 修饰符允许你控制由精确的系统修饰符组合触发的事件。

实例

```html
<!-- 即使 Alt 或 Shift 被一同按下时也会触发 -->
<button @click.ctrl="onClick">A</button>

<!-- 有且只有 Ctrl 被按下的时候才触发 -->
<button @click.ctrl.exact="onCtrlClick">A</button>

<!-- 没有任何系统修饰符被按下的时候才触发 -->
<button @click.exact="onClick">A</button>
```



## 表单



我们可以用 v-model 指令在表单 `<input>`、`<textarea>` 及 `<select>` 等元素上创建双向数据绑定。



v-model 会忽略所有表单元素的 value、checked、selected 属性的初始值，使用的是 data 选项中声明初始值。

v-model 在内部为不同的输入元素使用不同的属性并抛出不同的事件：

- text 和 textarea 元素使用 `value` 属性和 `input` 事件；
- checkbox 和 radio 使用 `checked` 属性和 `change` 事件；
- select 字段将 `value` 作为属性并将 `change` 作为事件。



### 输入框

```html
<input v-model="message"/>
<p>{{message}}</p>
```



### 复选框

复选框如果是一个为逻辑值，如果是多个则绑定到同一个数组：

```html
<p>单选框</p>
<input type="checkbox" id="checkbox" v-model="checked"/>
<label for="checkbox">{{checked}}</label>

<p>多个复选框：</p>
<input type="checkbox" id="runoob" value="Runoob" v-model="checkedNames">
<label for="runoob">Runoob</label>
·
·
·
```



### 单选按钮

```html
<input type="radio" id="runoob" value="Runoob" v-model="picked">
<label for="runoob">Runoob</label>
```



### select 列表

```
<select v-model="selected" name="fruit">
    <option value="">选择一个网站</option>
    <option value="www.runoob.com">Runoob</option>
</select>

<div id="output">
	选择的网站是: {{selected}}
</div>
```



### 值绑定



复选框 (Checkbox)：

```html
<input type="checkbox" v-model="toggle" true-value="yes" false-value="no" />

// 选中时
vm.toggle === 'yes'
// 取消选中 
vm.toggle === 'no'
```



**单选框 (Radio)：**

```html
<input type="radio" v-model="pick" v-bind:value="a" />
// 当选中时
vm.pick === vm.a
```



**选择框选项 (Select)：**

```html
<select v-model="selected">
  <!-- 内联对象字面量 -->
  <option :value="{ number: 123 }">123</option>
</select>
// 当被选中时
typeof vm.selected // => 'object'
vm.selected.number // => 123
```



### 修饰符

#### .lazy

在默认情况下， v-model 在 input 事件中同步输入框的值与数据，但你可以添加一个修饰符 lazy ，从而转变为在 change 事件中同步：

```html
<!-- 在 "change" 而不是 "input" 事件中更新 -->
<input v-model.lazy="msg" >
```



#### .number

如果想自动将用户的输入值转为 Number 类型（如果原值的转换结果为 NaN 则返回原值），可以添加一个修饰符 number 给 v-model 来处理输入值：

```html
<input v-model.number="age" type="number">
```



#### .trim

如果要自动过滤用户输入的首尾空格，可以添加 trim 修饰符到 v-model 上过滤输入：

```html
<input v-model.trim="msg">
```





## 自定义指令 directive



举例，注册一个全局指令 v-focus：

```html
<div id="app">
    <p>页面载入时，input 元素自动获取焦点：</p>
    <input v-focus>
</div>
 
<script>
const app = Vue.createApp({})
// 注册一个全局自定义指令 `v-focus`
app.directive('focus', {
  // 当被绑定的元素挂载到 DOM 中时……
  mounted(el) {
    // 聚焦元素
    el.focus()
  }
})
app.mount('#app')
</script>
```



我们也可以在实例使用 directives 选项来注册局部指令，这样指令只能在这个实例中使用：

```html
<div id="app">
    <p>页面载入时，input 元素自动获取焦点：</p>
    <input v-focus>
</div>
 
<script>
const app = {
   data() {
      return {
      }
   },
   directives: {
      focus: {
         // 指令的定义
         mounted(el) {
            el.focus()
         }
      }
   }
}
 
Vue.createApp(app).mount('#app')
```



### 钩子

#### 钩子函数

指令定义函数提供了几个钩子函数（可选）：

- `created `: 在绑定元素的属性或事件监听器被应用之前调用。
- `beforeMount `: 指令第一次绑定到元素并且在挂载父组件之前调用。。
- `mounted `: 在绑定元素的父组件被挂载后调用。。
- `beforeUpdate`: 在更新包含组件的 VNode 之前调用。。
- `updated`: 在包含组件的 VNode 及其子组件的 VNode 更新后调用。
- `beforeUnmount`: 当指令与元素解除绑定且父组件已卸载时，只调用一次。
- `unmounted`: 当指令与元素解除绑定且父组件已卸载时，只调用一次。

实例

```vue
import { createApp } from 'vue'
const app = createApp({})
 
// 注册
app.directive('my-directive', {
  // 指令是具有一组生命周期的钩子：
  // 在绑定元素的 attribute 或事件监听器被应用之前调用
  created() {},

  // 在绑定元素的父组件挂载之前调用
  beforeMount() {},

  // 绑定元素的父组件被挂载时调用
  mounted() {},

  // 在包含组件的 VNode 更新之前调用
  beforeUpdate() {},

  // 在包含组件的 VNode 及其子组件的 VNode 更新之后调用
  updated() {},

  // 在绑定元素的父组件卸载之前调用
  beforeUnmount() {},

  // 卸载绑定元素的父组件时调用
  unmounted() {}
})
 
// 注册 (功能指令)
app.directive('my-directive', () => {
  // 这将被作为 `mounted` 和 `updated` 调用
})
 
// getter, 如果已注册，则返回指令定义
const myDirective = app.directive('my-directive')
```



#### 钩子函数参数

钩子函数的参数有：

**el**

**el** 指令绑定到的元素。这可用于直接操作 DOM。

**binding**

binding 是一个对象，包含以下属性：

- `instance`：使用指令的组件实例。
- `value`：传递给指令的值。例如，在 `v-my-directive="1 + 1"` 中，该值为 `2`。
- `oldValue`：先前的值，仅在 `beforeUpdate` 和 `updated` 中可用。值是否已更改都可用。
- `arg`：参数传递给指令 (如果有)。例如在 `v-my-directive:foo` 中，arg 为 `"foo"`。
- `modifiers`：包含修饰符 (如果有) 的对象。例如在 `v-my-directive.foo.bar` 中，修饰符对象为 `{foo: true，bar: true}`。
- `dir`：一个对象，在注册指令时作为参数传递。



**vnode**

作为 el 参数收到的真实 DOM 元素的蓝图。



**prevNode**

上一个虚拟节点，仅在 beforeUpdate 和 updated 钩子中可用。





## 路由



通过 Vue 可以实现多视图的单页 Web 应用（single page web application，SPA）。

Vue.js 路由需要载入 [vue-router 库](https://github.com/vuejs/vue-router-next)

中文文档地址：[vue-router 文档](https://next.router.vuejs.org/zh/guide/)。



### router-link

创建连接。



### router-view

显示与 url 对应的组件。



实例

```vue
// 1. 定义路由组件.
// 也可以从其他文件导入
const Home = { template: '<div>Home</div>' }
const About = { template: '<div>About</div>' }
 
// 2. 定义一些路由
// 每个路由都需要映射到一个组件。
// 我们后面再讨论嵌套路由。
const routes = [
  { path: '/', component: Home },
  { path: '/about', component: About },
]
 
// 3. 创建路由实例并传递 `routes` 配置
// 你可以在这里输入更多的配置，但我们在这里
// 暂时保持简单
const router = VueRouter.createRouter({
  // 4. 内部提供了 history 模式的实现。为了简单起见，我们在这里使用 hash 模式。
  history: VueRouter.createWebHashHistory(),
  routes, // `routes: routes` 的缩写
})
 
// 5. 创建并挂载根实例
const app = Vue.createApp({})
//确保 _use_ 路由实例使
//整个应用支持路由。
app.use(router)
 
app.mount('#app')
 
// 现在，应用已经启动了！
```



点击过的导航链接都会加上样式 **class ="router-link-exact-active router-link-active"**。



### router-link 属性

#### to

表示目标路由的链接。

```html
<!-- 字符串 -->
<router-link to="home">Home</router-link>
<!-- 渲染结果 -->
<a href="home">Home</a>

<!-- 使用 v-bind 的 JS 表达式 -->
<router-link v-bind:to="'home'">Home</router-link>

<!-- 不写 v-bind 也可以，就像绑定别的属性一样 -->
<router-link :to="'home'">Home</router-link>

<!-- 同上 -->
<router-link :to="{ path: 'home' }">Home</router-link>

<!-- 命名的路由 -->
<router-link :to="{ name: 'user', params: { userId: 123 }}">User</router-link>

<!-- 带查询参数，下面的结果为 /register?plan=private -->
<router-link :to="{ path: 'register', query: { plan: 'private' }}">Register</router-link>
```



#### replace

导航后不会留下 history 记录。

```html
<router-link :to="{ path: '/abc'}" replace></router-link>
```



#### append

设置 append 属性后，则在当前 (相对) 路径前添加其路径。

```html
<router-link :to="{ path: 'relative/path'}" append></router-link>
```



#### tag

使用 `tag` prop 类指定何种标签，同样它还是会监听点击，触发导航。

```html
<router-link to="/foo" tag="li">foo</router-link>
<!-- 渲染结果 -->
<li>foo</li>
```



#### active-class

设置链接激活时使用的 CSS 类名。可以通过以下代码替代。

```html
<style>
   ._active{
      background-color : red;
   }
</style>
<p>
   <router-link v-bind:to = "{ path: '/route1'}" active-class = "_active">Router Link 1</router-link>
   <router-link v-bind:to = "{ path: '/route2'}" tag = "span">Router Link 2</router-link>
</p>
```



注意这里 **class** 使用 **active-class="_active"**。



#### exact-active-class

配置当链接被精确匹配的时候应该激活的 class。可以通过以下代码来替代。

```html
<p>
   <router-link v-bind:to = "{ path: '/route1'}" exact-active-class = "_active">Router Link 1</router-link>
   <router-link v-bind:to = "{ path: '/route2'}" tag = "span">Router Link 2</router-link>
</p>
```



### event

声明可以用来触发导航的事件。可以是一个字符串或是一个包含字符串的数组。

```html
<router-link v-bind:to = "{ path: '/route1'}" event = "mouseover">Router Link 1</router-link>
```

以上代码设置了 event 为 mouseover ，及在鼠标移动到 Router Link 1 上时导航的 HTML 内容会发生改变。

