优秀开源项目



# 201~25

1. [xiaozhi-esp32](https://gitee.com/blue-print/xiaozhi-esp32) - 小智 AI 聊天机器人。
2. [LLaMA-Factory](https://github.com/hiyouga/LLaMA-Factory) - 使用零代码命令行与 Web UI 轻松微调百余种大模型。
3. [res-downloader](https://github.com/putyy/res-downloader) - 支持微信视频号、小程序、抖音、快手、小红书、酷狗音乐、qq音乐等网络资源下载。
4. [freeCodeCamp](https://github.com/freeCodeCamp/freeCodeCamp) - 开源代码库和课程。学习免费编程。
5. [video-subtitle-extractor](https://github.com/YaoFANGUK/video-subtitle-extractor) - 视频硬字幕提取，生成srt文件。无需申请第三方API，本地实现文本识别。
6. [video-subtitle-remover](https://github.com/YaoFANGUK/video-subtitle-remover) - 基于AI的图片/视频硬字幕去除、文本水印去除，无损分辨率生成去字幕、去水印后的图片/视频文件。
7. [auto_ai_subtitle](https://github.com/qinL-cdy/auto_ai_subtitle) - AI 字幕生成。
8. [TikTokDownloader](https://github.com/JoeanAmier/TikTokDownloader) - 批量下载抖音账号里的作品内容。
9. [GitHub-Chinese-Top-Charts](https://github.com/GrowingGit/GitHub-Chinese-Top-Charts) - github 中文排行榜。
10. [InstantID](https://github.com/InstantID/InstantID) - 使用 AI 生成个性化图片，[工具](https://huggingface.co/spaces/InstantX/InstantID)。
11. XAgent 是一个开源的基于大型语言模型（LLM）的自主智能体，可以自动解决各种任务。[gitee](https://gitee.com/TappaT/XAgent)，[github](https://github.com/OpenBMB/XAgent)
12. [wenda](https://github.com/l15y/wenda) - 闻达：一个 LLM 调用平台。为小模型外挂知识库查找和设计自动执行动作，实现不亚于于大模型的生成能力。
13. [screenshot-to-code](https://github.com/abi/screenshot-to-code) - 放入屏幕截图并将其转换为干净的 HTML/Tailwind/JS 代码。



# 151 ~ 200

1. [pure-live-core](https://github.com/iyear/pure-live-core) - 国人在 GitHub 上开源了一个直播系统，简单纯粹，支持直播、弹幕等功能。跨平台，甚至可以运行在路由器上。
2. [sealos](https://github.com/fanux/sealos) - 一款开源的 k8s 部署工具，一条命令离线安装高可用 kubernetes，3min 装完，700M，100 年证书，版本不要太全，生产环境稳如老狗。
3. [MHDDoS](https://github.com/MatrixTM/MHDDoS) - 一个具有 51 种攻击方法的 Python3 DDoS 开源脚本。
4. [Amazing-QR](https://github.com/x-hw/amazing-qr) - 可生成普通二维码、带图片的艺术二维码（黑白与彩色）、动态二维码（黑白与彩色）。
5. [AI-RecommenderSystem](https://github.com/zhongqiangwu960812/AI-RecommenderSystem) - 经典算法模型，包括经典算法模型和技术，各类工具、数据集的参考学习，并尝试用浅显易懂的语言，把每个模型或者算法解释清楚。
6. [herzbeat](https://github.com/dromara/hertzbeat) - 易用友好的云监控系统。拥有自定义监控能力，无需Agent。网站监测，PING连通性，端口可用性，数据库，操作系统，API监控，阈值告警，告警通知(邮件微信钉钉飞书)。【java】
7. [funNLP](https://github.com/fighting41love/funNLP) - funNLP，几乎最全的中文 NLP 资源库。包括中英文敏感词、语言检测、中外手机 / 电话归属地 / 运营商查询、名字推断性别、手机号抽取、身份证抽取等内容。
8. [fantastic-admin](https://github.com/hooray/fantastic-admin) - 一款开箱即用的 Vue 中后台管理系统框架，拥有多种布局与主题，动效丰富，可灵活定制，并支持多语言、多级路由缓存、自动生成导航栏等功能。
9. [amplication](https://github.com/amplication/amplication) - 一个开源的开发者工具，可用于快速构建高质量的前后端项目，提升开发效率。功能包括可视化数据模型管理，自动化批量代码生成，管理面板搭建，生成满足 CRUD 需求的 REST 和 GraphQL API 等。
10. [ColossalAI](https://github.com/hpcaitech/ColossalAI) - 可用于 AI 大规模并行训练，仅需一半数量的 GPU，便能完成相同效果的 GPT-3 训练工作，极大降低了项目研发成本！
11. [JavaScript Visualizer](https://github.com/Hopding/js-visualizer-9000-client) - JavaScript 可视化调试工具，运行后，可动态展示任务队列、调用栈、Event Loop 等过程与结果。
12. [nlp-notebook](https://github.com/jasoncao11/nlp-notebook) - NLP 领域常见任务的实现，包括新词发现、以及基于 pytorch 的词向量、中文文本分类、实体识别、摘要文本生成、句子相似度判断、三元组抽取、预训练模型等。
13. [rembg](https://github.com/danielgatis/rembg) - 简单实用的删除图像背景/抠图工具。
14. [tiptop](https://github.com/nschloe/tiptop) - 炫酷的命令行系统监控工具。
15. [lookscanned](https://github.com/rwv/lookscanned.io) - 一个可以将 PDF 转换成扫描文件的开源工具，支持设置文件角度、噪点、模糊度等属性，整个过程无需用到打印机和扫描仪。
16. [hashcat](https://github.com/hashcat/hashcat) - 一款强大的密码恢复工具。破解速度超快支持多种算法，适用于 Linux、macOS 和 Windows 操作系统。
17. [AI-Paper-collector](https://github.com/MLNLP-World/AI-Paper-collector) - 可用于查找包含指定关键词的顶会论文列表，涵盖了 2019-2021 自然语言处理，计算机视觉，信息检索等领域的顶会论文。
18. [区块链黑暗森林自救手册](https://github.com/slowmist/Blockchain-dark-forest-selfguard-handbook) - 主要介绍了关于区块链安全相关的攻击手段、技术防控知识等内容。
19. [Json Hero](https://github.com/jsonhero-io/jsonhero-web) - 以列表视图、树状视图、编辑器视图等多种方式来查看 JSON 文件，并自动识别数据内容，展示数据预览。
20. [PP-Matting](https://github.com/PaddlePaddle/PaddleSeg/tree/release/2.5/Matting) - 百度智能抠图算法，完美保留发丝、树叶等精细边缘。
21. [C++ Concurrency in Action 2ed 笔记](https://github.com/downdemo/Cpp-Concurrency-in-Action-2ed) - C++11/14/17/20 多线程，掌握操作系统原理，解锁并发编程技术。
22. [NLP 论文列表](https://github.com/DengBoCong/nlp-paper) - 字节跳动工程师在 GitHub 上整理的 NLP 论文列表（附阅读笔记），收录了一些比较经典或思路新奇的论文。
23. [GDB dashboard](https://github.com/cyrus-and/gdb-dashboard) - Python 调试工具。
24. [pyscript](https://github.com/pyscript/pyscript) - 简单添加几行代码，便能在 HTML 中内嵌 Python 代码，并在浏览器上运行。
25. [best-of-ml-python](https://github.com/ml-tooling/best-of-ml-python) - 收录项目共计 900 多个，包括数据可视化、自然语言处理、文本与图像数据、Web 爬虫等分类。
26. [nlp-paper](https://github.com/DengBoCong/nlp-paper) - 字节跳动工程师在 GitHub 上整理的 NLP 论文列表（附阅读笔记），收录了一些比较经典或思路新奇的论文。
27. [gdb-dashboard](https://github.com/cyrus-and/gdb-dashboard) - 开源的 Python 调试工具：GDB dashboard，为 Python GDB 提供了可视化界面，更清晰的显示了程序调试信息，使开发者能专注于 Python 控制流。
28. [sql-masterclass](https://github.com/DataWithDanny/sql-masterclass) - 《给数据科学家看的 SQL 教程》，作者将基于一个加密货币交易数据库，带你了解 SQL 的常见用法。
29. [raspberry_tutorial](https://github.com/wxlscm/raspberry_tutorial) - 《树莓派编程实用教程》，全书共 31000 字，提供 20 多个树莓派应用实例，适合想在树莓派开发项目的工程师、学生参考学习。
30. [cnchar](https://github.com/theajack/cnchar) - 汉字拼音笔画库。功能非常齐全，支持汉字拼音、笔画数解析，多种模式绘制汉字，并能完成语音识别、语音合成等操作。
31. [modren](https://github.com/RudraSwat/modren) - 一个开源的 Linux 应用商店。安装成功后，便可通过该商店下载、安装各类 Linux 应用。支持 APT、Snap、Flatpaks、DEB 等多种格式的安装包，可用于 Ubuntu、Debian 衍生发行版等系统。
32. [Awesome-Redteam](https://github.com/Threekiii/Awesome-Redteam) - 红队知识仓库。里面收录了一系列网络安全相关的技术文档、开发工具、公开知识库等内容，覆盖漏洞研究、信息收集、内网渗透等领域。
33. [watermark-removal](https://github.com/zuruoke/watermark-removal) - 基于机器学习的图像修复方法，自动去除图片水印。
34. [2022 年开源操作系统训练营](https://github.com/LearningOS/rust-based-os-comp2022) - 教程共分为八章，主要展示如何从零开始，用 Rust 语言写一个基于 RISC-V 架构的 类 Unix 内核 。
35. [操作系统导论](https://github.com/remzi-arpacidusseau/ostep-translations) - 中文本教程。
36. [lensm](https://github.com/loov/lensm) - 一款基于 Go 语言开发的汇编及源代码浏览工具，可用于项目性能优化、调试等场景。
37. [PrettyMaps](https://github.com/marceloprates/prettymaps) - 基于 OpenStreetMap 数据，帮助开发者快速绘制简洁美观的地图。
38. [magical_spider](https://github.com/lixi5338619/magical_spider) - Web 端站点的采集方案。爬虫工具
39. [lama-cleaner](https://github.com/Sanster/lama-cleaner) - 开源的图像修复工具，可用于快速去除图像中各种物品、人物、字体、水印等对象，并支持老照片修复、文本替换图像内容等。
40. [ModelScope](https://modelscope.cn/home) -魔搭社区【阿里达摩院】，汇聚各领域最先进的机器学习模型，提供模型探索体验、推理、训练、部署和应用的一站式服务。
49. [MiniGPT-4](https://github.com/Vision-CAIR/MiniGPT-4) - 用高级大型语言模型增强视觉语言理解。







# 101~150

1. [pulse](https://github.com/adamian98/pulse) - 通过生成模型的潜在空间探索进行自监督照片上采样。
2. [TecoGAN](https://github.com/thunil/TecoGAN) - 视频，图片去除马赛克工具，提升图片、视频清晰度。
3. [CS-Notes](https://github.com/CyC2018/CS-Notes) - 技术面试必备基础知识、Leetcode、计算机操作系统、计算机网络、系统设计。
4. [免费的编程中文书籍索引](https://github.com/justjavac/free-programming-books-zh_CN)
5. [LeetCode](https://github.com/begeekmyfriend/leetcode) - 基于纯 C 语言实现的 LeetCode 题解仓库，共有 200 多道题，涵盖链表、队列、堆栈、递归、动态规划、二叉搜索树等知识点。
6. [清华大学计算机系课程攻略](https://github.com/PKUanonym/REKCARC-TSC-UHT) - 覆盖了计算机科学导论、数据结构、人工智能导论、计算机组成原理、操作系统、现代密码学、线性代数、微积分等多个领域的资料、课件、参考教材等内容。[book](https://rekcarc-tsc-uht.readthedocs.io/en/latest/)
7. [RedisJSON](https://github.com/RedisJSON/RedisJSON) - 允许开发者从 Redis 中存储、更新、获取 JSON 值。
8. [ego](https://github.com/gotomicro/ego) - 开源的微服务框架：ego，集成了各种工程实践，通过组件化设计模式，让业务能统一调用与启动组件。
9. [在拥挤和变化的世界中茁壮成长: C++ 2006–2020](https://github.com/Cpp-Club/Cxx_HOPL4_zh) - 论文。C++ 之父此前在 HOPL 会议上发布了一篇技术论文：《在拥挤和变化的世界中茁壮成长：C++ 2006–2020》。
10. [CoNote](https://github.com/phith0n/conote-community) - 综合安全测试平台：CoNote，可以让渗透测试过程变得更加方便。支持命令执行漏洞利用、SSRF 漏洞探测、SQL 注入、DNS 记录等功能，还可以向内网或无 HTTP 服务的平台传输数据与文件。
11. [wumei-smart](https://github.com/kerwincui/wumei-smart) - **是一套开源的软硬件系统，可用于二次开发和学习，快速搭建自己的智能家居系统。** 硬件工程师可以把自己的设备集成到系统；软件工程师可以使用项目中的设备熟悉软硬件交互。
12. [easychen/pushdeer](https://github.com/easychen/pushdeer) - PushDeer 开源版，可以自行架设的无 APP 推送服务。
13. [ellisonleao/magictools](https://github.com/ellisonleao/magictools) - 开源的游戏开发教程：Magic Tools，整理了游戏开发相关的工具、美术素材、项目源码、计算机图形学等资源。[中文版](https://indienova.com/sp/gameDevResource)
14. [alexmojaki/futurecoder](https://github.com/alexmojaki/futurecoder) - 开源的可交互式 Python 教程：Future Coder，面向初学者设计，亮点是可以一边在线学习技术，一边敲代码验证结果，强化知识的吸收。
15. [pywebio/PyWebIO](https://github.com/pywebio/PyWebIO) - 通过一系列命令式的交互函数，在浏览器上获取用户的输入数据并输出。让你无需编写 HTML 和 JS 代码，即可构建一个小型 Web 应用程序。
16. [JDFED/leo](https://github.com/JDFED/leo) - 京东前端技术团队在 GitHub 开源了一款前端脚手架工具：leo。覆盖了前端开发全链路、可扩展、可定制的终端运行功能，并支持模板、构建器、扩展命令等丰富的周边生态扩展。
17. [niubiqigai/turtle](https://github.com/niubiqigai/turtle) - 汇总了 12 个小型 Python 项目，其中包括圣诞树绘制、弹簧隧道、贪吃蛇游戏代码等项目，适合新手用于日常练习。
18. [算法通关手册](https://github.com/itcharge/LeetCode-Py) - 算法与数据结构，基础知识的讲解教程【LeetCode】650+ 道题目的详细解析。
19. [Mall-Cook](https://github.com/wangyuan389/mall-cook) - 一个基于vue开发的可视化商城搭建平台，包括多页面可视化构建、Json Schema 生成器（可视化搭建物料控制面板），实现组件流水线式标准接入平台。
20. [ddddocr](https://github.com/sml2h3/ddddocr) - OCR 通用验证码识别 SDK 免费开源版
21. [深度学习 500 问](https://github.com/shliang0603/Awesome-DeepLearning-500FAQ) - 深度学习 500 问，以问答形式对常用的概率知识、线性代数、机器学习、深度学习、计算机视觉等热点问题进行阐述，以帮助自己及有需要的读者。 全书分为 18 个章节，50 余万字。
22. [Hitomi-Downloader](https://github.com/KurtBestor/Hitomi-Downloader) - 可用于快速下载各大网站的视频、漫画、电影、音乐等内容。拥有简洁易用的 UI 界面，多线程及脚本支持。
23. [Real-CUGAN](https://github.com/bilibili/ailab/tree/main/Real-CUGAN) - 视频降噪工具。
24. [skywind3000/preserve-cd](https://github.com/skywind3000/preserve-cd) - 绝版游戏保护工程。
25. [grist](https://github.com/gristlabs/grist-core) - 现代化电子表格。
26. [TerraForge3D](https://github.com/Jaysmito101/TerraForge3D) - 专业的地形生成和建模工具。
27. [saikou](https://github.com/saikou-app/saikou) - 一个只支持 anilist 的客户端，可以让你流媒体下载动画和漫画。
28. [JavaSecInterview](https://github.com/4ra1n/JavaSecInterview) - Java 安全研究与安全开发面试题总结。
29. [mermaid](https://github.com/mermaid-js/mermaid) - 绘制各类流程图、甘特图、序列图、关系图等图表。
30. [sha256algorithm](github.com/dmarman/sha256algorithm) - 通过可视化的方式，让你更为直观的理解 SHA256 算法原理。
31. [vue-vben-admin](https://github.com/vbenjs/vue-vben-admin) - 开源的管理后台模板：Vue vben admin，基于 Vue 构建，开箱即用，拥有多款可配置主题，内置 Mock 数据方案、动态路由权限生成方案。
32. [FastFold](https://github.com/hpcaitech/FastFold) - 训练推理加速方案【潞晨科技】。
33. [greppo](https://github.com/greppo-io/greppo) - 可快速搭建一款可交互式的「地理空间」应用。【python】
34. [Linux 系统操作手册](https://github.com/abarrak/linux-sysops-handbook) - 书中包括系统用户权限管理、Shell 使用技巧、文件权限设置、后台管理任务设置、日志记录的监控与分析等知识。【书】
35. [db-tutorial](https://github.com/dunwu/db-tutorial) - 里面含有 MySQL、NoSQL、MongoDB、Elasticsearch、Redis 等学习教程，其中 MySQL 与 Redis 提供了思维导图对学习重点内容进行梳理，以便大家更好学习理解。[gitee](https://gitee.com/turnon/db-tutorial)，[电子书](https://turnon.gitee.io/db-tutorial/)
36. [mysql-tutorial](https://github.com/jaywcjlove/mysql-tutorial) - MySQL入门教程。[21分钟MySQL基础入门](https://github.com/jaywcjlove/mysql-tutorial/blob/master/21-minutes-MySQL-basic-entry.md)
37. [Rust 数据结构与算法](https://github.com/QMHTMY/RustBook) - 共分九章，主要介绍计算机科学、基本数据结构与算法分析、递归、查找、排序等内容。【书】
38. [yao](https://github.com/YaoApp/yao) - 开源低代码应用引擎，无需编写一行代码，即可快速创建 Web 服务和管理后台，大幅解放生产力。【golang】
39. [Python 设计模式](https://github.com/faif/python-patterns) - 作者通过编码实现，为多种 Python 设计模式提供了代码参考示例。
40. [Plasticity](https://github.com/nkallen/plasticity) - 3D 建模工具。
41. [Rust 练习实践](https://github.com/sunface/rust-by-practice) - 该教程将通过简单到困难的代码示例，让你学习如何基于小型项目来练习和实践 Rust。目前有提供中、英文两个版本。
42. [机器学习系统：设计和实现](https://github.com/openmlsys/openmlsys-zh) - 主要讲解现代机器学习系统的设计原理以及实现经验。覆盖了编程接口、计算图、编译器前后端、数据处理、模型部署、分布式训练等知识点。
43. [区块链开发指南](https://github.com/dcbuild3r/blockchain-development-guide) - 该教程主要讲解 Web 前后端开发、以太坊基础、智能合约、密码学、数据分析等知识。
44. [中文语音语料](https://github.com/fighting41love/zhvoice) - 里面包含了 8 个开源数据集，3200 个说话人，900 小时语音，1300 万字。
45. [ecapture](https://github.com/ehids/ecapture) - 一款无需 CA 证书，就可以进行 HTTPS 通讯明文抓包的工具。
46. [用 Rust 开发一个操作系统](https://github.com/phil-opp/blog_os) - [在线阅读](https://os.phil-opp.com/zh-CN/)，通过这个教程，你将学习如何使用 Rust 来编写一个操作系统。
47. [regex-vis](https://github.com/Bowen7/regex-vis) - 正则表达式可视化工具。
48. [SystemSix](https://github.com/EngineersNeedArt/SystemSix) - 一款可在树莓派上运行的电子水墨屏，可用于显示日历、天气、操作系统界面等内容。
49. [蘑菇书EasyRL](https://github.com/datawhalechina/easy-rl) - 覆盖了强化学习、马尔可夫决策过程、策略梯度、模仿学习等多个知识点。【书】
50. [virtual_try_on_use_deep_learning](https://github.com/hpc203/virtual_try_on_use_deep_learning) - 使用深度学习算法实现虚拟试衣，结合了人体姿态估计、人体分割、几何匹配和 GAN，四种模型。仅仅只依赖 opencv 库就能运行。



# 51~100

1. [freeCodeCamp](https://github.com/freeCodeCamp/freeCodeCamp) - 一个自由开源的学习编程的社区，致力于帮助人们利用零散时间学习编程。
2. [frontend-dev-bookmarks](https://github.com/dypsilon/frontend-dev-bookmarks) - 为前端Web开发人员提供的资源。
3. [tech-interview-handbook](https://github.com/yangshun/tech-interview-handbook) - 对于繁忙的工程师，选定的面试准备工具。
4. CleanUp.pictures - 可通过画笔，在线擦除图像中的物品对象。[github](github.com/initml/cleanup.pictures) [官网](https://cleanup.pictures/)
5. [Cpp-0-1-Resource](github.com/AnkerLeng/Cpp-0-1-Resource) - C++ 入门学习资源。
6. [algorithm-stone](github.com/acm-clan/algorithm-stone) - ACM / LeetCode 算法竞赛路线图：《算法・进阶石》，包含了红黑树、动态规划、堆栈队列、链表、二分查找、几何问题等内容。
7. [Git 飞行规则](git.io/J6XZN) - Git 学习指南。
8. [lama](github.com/saic-mdal/lama) - 快速进行图像修复、指定对象移除等操作。
9. [N64Wasm](github.com/nbarkhina/N64Wasm) - 任天堂 64 模拟器（N64）。
10. [同构 - 编程中的数学](github.com/liuxinyu95/unplugged) - 书，主讲自然数和计算机程序、欧几里得算法、递归的形式与结构、斐波那契数列、罗素悖论、数学基础的分歧等内容。
11. [Financial-Knowledge-Graphs](github.com/jm199504/Financial-Knowledge-Graphs) - 小型金融知识图谱构建流程。
12. [A Complete Machine Learning Package](github.com/Nyandwi/machine_learning_complete) - 全面的机器学习资源，覆盖 Python 编程、数据分析与可视化、计算机视觉、开发工具、最佳实践等内容。
13. [数学建模学习资源](github.com/zhanwen/MathModel) - 主要包含数学建模相关的竞赛优秀论文、算法、LaTeX 论文模板、算法思维导图、书籍、Matlab 教程等内容。
14. [自己动手写编译器](github.com/pandolia/tinyc) - 书，绍如何通过实现一个简单的编译器（TinyC），并借助实例来描述基本的编译原理及过程。
15. [rawgraphs-app](github.com/rawgraphs/rawgraphs-app) - 数据可视化工具。
16. [Dive Into Systems](https://diveintosystems.org/) - 一本计算机系统教程，教程将从 C 语言展开，带你了解 C 语言调试工具、二进制、冯诺依曼结构、汇编、代码优化、共享内存等相关知识。
17. [vulmap](github.com/zhzyker/vulmap) - 可对 Web 容器、Web 服务器、Web 中间件以及 CMS 等 Web 程序进行漏洞扫描，并且具备漏洞利用功能。
18. [NETworkManager](github.com/BornToBeRoot/NETworkManager) - 源的 Windows 网络管理工具：NETworkManager，拥有 IP 扫描、端口扫描、路由跟踪、DNS 查询等功能。
19. [PathPlanning](github.com/zhm-real/PathPlanning) - 动画算法库：PathPlanning，主要实现多种在机器人领域中，常用到的路径规划算法，其中也包括基于搜索与采样的方法。
20. [Python Tutor](https://pythontutor.com/) - 代码可视化网站，可将代码按执行步骤，分段可视化展示，并允许将可视化代码内嵌到网站上。目前已支持 Python、Java、C、C++、JavaScript、Ruby 等编程语言，该工具可应用到文档教程、技术讲座、团队新手指引项目等场景上。
21. [Semi Design](https://semi.design/) - 由字节跳动抖音前端与 UED 团队设计、开发并维护，包含设计语言、React 组件、主题等开箱即用的中后台解决方案，帮助设计师与开发者打造高质量产品。[github](https://github.com/DouyinFE/semi-design)
22. [SQLMap](https://github.com/sqlmapproject/sqlmap) - github 上比较实用的 SQL 渗透测试工具。
23. [wLogger](github.com/jyolo/wLogger) - 一款集日志采集、日志解析持久化存储、Web 流量实时监控，三位一体的 Web 服务流量监控应用。
24. [AI Writer](https://github.com/BlinkDL/AI-Writer) - 可用 GPT 来生成中文网文小说，模型训练数据来自网文。
25. [ShotCut](https://www.shotcut.org/) - [github](https://github.com/mltframework/shotcut) - 免费开源的视频编辑器：Shotcut，兼容 Windows、macOS、Linux 等主流操作系统。
26. [ShareX](https://github.com/ShareX/ShareX) - Windows 截图与录屏工具，支持截图、录屏、OCR 文本识别、图像水印添加、内容上传、地址分享、颜色调整、图像编辑、视频格式转换等功能。[官网](https://getsharex.com/)
27. [ChainKnowledgeGraph](https://github.com/liuhuanyong/ChainKnowledgeGraph) - 产业链图谱，通过数据处理、抽取等方式，建成图谱规模数十万，涵盖上市公司所属行业关系、行业上级关系、产品上游原材料、下游产品关系、公司主营产品、产品小类等 6 大块内容。
28. [PaddleGAN](https://github.com/PaddlePaddle/PaddleGAN) - 百度开源的一款 AI 生成对抗网络开发套件，拥有风格迁移、人脸换妆、照片动漫化、人脸表情迁移、人脸生成等多个模型。
29. [C++ 11 全套设计模式](https://github.com/jaredtao/DesignPattern) - 包含了工厂方法模型、单例模式、策略模式、观察者模式等 23 种模式的常见用法。
30. [KalidoKit](https://github.com/yeemachine/kalidokit) - 人体运动模拟器，通过捕捉人体面部表情、眼睛、姿势、手部动作等元素后，便可自动映射到虚拟人物身上，打通真实世界与虚拟世界的连接。
31. [TinyWebServer](https://github.com/qinguoyi/TinyWebServer) - 可应用于 Linux 系统下的一款轻量级 Web 服务器，主要目的是帮助新手开发者快速实践网络编程，搭建属于自己的服务器。
32. [Web 在线客服系统](https://github.com/taoshihan1991/go-fly) - 项目基于 Go 语言与 MySQL 实现。
33. [AnimeGANv2-PyTorch](https://github.com/bryandlee/animegan2-pytorch) - 可借助 AI 技术，快速将图像转换为漫画风格画作，提供 Demo 试用。[demo](https://huggingface.co/spaces/akhaliq/AnimeGANv2)
34. [Rhubarb Lip Sync](https://github.com/DanielSWolf/rhubarb-lip-sync) - 可通过真实录音，让动漫人物的嘴唇实现音画同步。
35. [计算机安全和互联网](https://people.scs.carleton.ca/~paulv/toolsjewels.html) - 主要讲解计算机安全概念与基本原则、身份验证协议、操作系统安全和访问控制、Web 与浏览器安全、软件漏洞利用与提权等内容。书[英文]。
36. [CaoGuo](https://github.com/xxjwxc/caoguo) - 基于 Go 语言开发的小程序电商平台，支持商品选择、详情、下单、物流信息展示等功能。
37. [WantWords](https://github.com/thunlp/WantWords) - 通过指定词义，便可匹配与之描述相近的词汇。比如，当你输入「开心的词」，该工具便会给出 "快乐"、"愉快"、"高兴" 等词汇。[官网](https://wantwords.thunlp.org/)
38. [Spider Flow](https://github.com/ssssssss-team/spider-flow) - 一个高度灵活可配置的爬虫平台，用户无需编写代码，以流程图的方式，即可实现爬虫。
39. [useful-sed](https://github.com/adrianscheff/useful-sed) - Linux sed 命令行常用汇总，收集了 sed 命令行的诸多常见用法，可用于快速处理文本文件。
40. [分布式系统模式](https://github.com/dreamhead/patterns-of-distributed-systems)
41. [现代化计算机科学自学指南](https://git.io/J1EmD) - 指南中列举了计算机科学的九大经典学科，包括计算机系统结构、算法与数据结构、操作系统、计算机网络、数据库等计算机领域相关的经典书籍与教学视频。[github](https://github.com/izackwu/TeachYourselfCS-CN/blob/master/TeachYourselfCS-CN.md)
42. [Web Attack Cheat Sheet](https://github.com/riramar/Web-Attack-Cheat-Sheet) -  Web 攻击速查表,覆盖了 DNS 和 HTTP 检测、视觉识别、静态应用安全测试、漏洞搜索、SQL 注入、SSRF（服务器端请求伪造）等技术点。
43. [C/C++ 程序设计](https://github.com/ShiqiYu/CPP) - 作者为南方科技大学计算机系于仕琪副教授，同时也是 OpenCV 中国团队负责人。教程共分为 15 个篇章，覆盖 C/C++ 基础介绍、数据类型与算术运算法、数据结构、内存指针、函数、类等多个知识点的讲解。
44. [Translators](https://github.com/UlionTse/translators) - Python 翻译工具库，该库集成了谷歌、必应、有道、百度等多个翻译平台 API，支持上百种语言翻译，使用便捷，配置灵活。
45. [清华大学计算机学科推荐学术会议和期刊列表](https://github.com/bugaosuni59/TH-CPL) - 覆盖了高性能计算、计算机网络、网络与信息安全、理论计算机科学、系统软件与软件工程、数据库与数据挖掘、人工智能与模式识别、计算机图形学与多媒体等领域。
46. [基于深度学习和行人重识别](https://github.com/michuanhaohao/ReID_tutorial_slides) - 浙江大学罗浩博士开放的一个技术教程，课程主要包括深度学习基础、行人重识别理论基础和行人重识别代码实践三个篇章。内容包含各类课件与视频教程。
47. [file-online-preview](https://github.com/kekingcn/kkFileView) - 文件文档在线预览项目，支持主流办公文档的在线预览，如 doc、docx、Excel、pdf、txt、zip、rar、 图片等格式。该项目基于 Spring Boot 框架开发，提供 RESTful 接口。
48. [LeetCode company wise questions](https://github.com/MysteryVaibhav/leetcode_company_wise_questions) - 把众多知名大厂的 LeetCode 题目整合到一起，并打包成了 PDF，便于大家学习。
49. [Qlib](https://github.com/microsoft/qlib) - 微软在 GitHub 上开源的一个 AI 量化交易平台。
50. [Depix](https://github.com/beurtschipper/Depix) - 是一种从像素化屏幕截图中恢复密码的工具。去除马赛克，提高图片清晰度。





# 01~50

1. [X-TRACK](https://github.com/FASTSHIFT/X-TRACK) - 开源 GPS 自行车码表。
2. [TypeScript Deep Dive](https://github.com/jkchao/typescript-book-chinese) - 【深入理解 TypeScript】中译版。
3. [bigdata_analyse](https://github.com/TurboWay/bigdata_analyse) - 国人开源的数据分析项目库。
4. [Getting Things Done with PyTorch](https://github.com/curiousily/Getting-Things-Done-with-Pytorch) - 电子书，开发者可从该书中学到有关 PyTorch、神经网络、图像分类、人脸检测、情感分析等基础知识。
5. [chineseocr_lite](https://github.com/DayBreak-u/chineseocr_lite) - 一款超轻量级中文 OCR，支持竖排文字识别。
6. [1loc](https://github.com/phuoc-ng/1loc) - 【JavaScript】该项目收集了共计 126 个代码片段，其中包含对数组、日期、函数、DOM 等常见代码模块的组件实现。
7. [SZT-bigdata](https://github.com/geekyouth/SZT-bigdata) - 深圳地铁大数据客流分析系统。
8. [sparrow](https://github.com/sparrow-js/sparrow) - sparrow 的核心目标仅有一条 提升研发效率，目前提供基于 vue、element-ui 组件库中后台项目的实践，实时输出源代码。
9. [label-studio](https://github.com/heartexlabs/label-studio) - 开源的数据标注神器：Label Studio，可用于标注音视频、文本、图像、时间序列等数据类型，并导出为多种模型格式。
13. [raspberry-pi-os](https://github.com/s-matyukevich/raspberry-pi-os) - 使用 Linux 内核和 Raspberry Pi 学习操作系统开发。
14. [bpytop](https://github.com/aristocratos/bpytop) - 资源监视器，显示处理器、内存、磁盘、网络和进程的使用情况和统计信息。
15. [Real-ESRGAN](https://github.com/xinntao/Real-ESRGAN) - 通用图像恢复算法。【推荐】
16. [GFPGAN](https://github.com/TencentARC/GFPGAN) - 真实人脸恢复算法。【推荐】
17. [StyleCariGAN](https://github.com/wonjongg/StyleCariGAN) - 通过 StyleGAN 要素地图调制生成漫画。
18. [RobustVideoMatting](https://github.com/PeterL1n/RobustVideoMatting) - 实时人像抠图算法。【来自字节跳动】
19. [theatre.js](https://github.com/AriaMinaei/theatre) - 前端动画设计库。
20. [btop](https://github.com/aristocratos/btop) - 开源的 Linux / OSX / FreeBSD 资源监控工具。
21. [darling](github.com/darlinghq/darling) - 可让你在 Linux 上无缝运行 macOS 系统软件。[官网](https://www.darlinghq.org/)
22. [深度学习在图像处理中的应用教程](https://github.com/WZMIAOMIAO/deep-learning-for-image-processing)
23. [图解 React 源码系列](github.com/7kms/react-illustration-series)
24. [Practical Go Lessons](https://www.practical-go-lessons.com/) - 通过这个教程，循序渐进的带你了解 Go 语言的基础知识，以及常见的计算机科学概念。主要包括 Go 语言简史、开发环境设置、基础语法、单元测试、并发、HTTP 客户端开发等内容。【英文】
25. 带你入门前端工程【书】 - [gitee](https://woai3c.gitee.io/introduction-to-front-end-engineering/) [github](https://github.com/woai3c/introduction-to-front-end-engineering)
26. [tangram](https://github.com/tangramdotdev/tangram) - 一个可帮助 AI 开发者快速训练、部署与监控机器学习模型的开源工具。
27. [LogicFlow](github.com/didi/LogicFlow) - 流程可视化的前端框架。【滴滴】[官网](http://logic-flow.org/)
28. [PaddleX](https://github.com/PaddlePaddle/PaddleX) - 全流程开发套件。
29. [flipper](github.com/facebook/flipper) - 移动端调试工具，可在 PC 桌面上可视化调试与控制 iOS、Android 与 React Native 应用。
30. [quickemu](github.com/wimpysworld/quickemu) - 虚拟机安装工具。
31. [服务器编程指南](https://github.com/howardlau1999/server-programming-guide) - 主要讲解服务器编程中的程序编译装载、C++ 项目构建、多线程与多进程编程范式使用、程序调试与部署等内容。
32. [剑指前端 Offer](https://github.com/hzfe/awesome-interview) - 前端面试库。
33. [uptime-kuma](github.com/louislam/uptime-kuma) - 网站监控工具。
34. [给初学者看的数据科学 (Data Science for Beginners)](github.com/microsoft/Data-Science-For-Beginners)- 主讲数据科学通用定义、统计与概率导论、使用 SQL 与 NoSQL 处理数据、Python 与数据可视化等知识。【微软】
35. [Fast-Drone-250](https://github.com/ZJU-FAST-Lab/Fast-Drone-250) - 浙江大学 FASTLAB 实验室 - 自主导航无人机的硬件组成与搭建方案。
36. [LogiCommon](github.com/didi/LogiCommon ) - Java 版认证、鉴权、管理、任务调度通用功能组件。【滴滴】
37. [Penetration Testing POC](https://github.com/Mr-xn/Penetration_Testing_POC) - 渗透测试笔记。
38. [taskcafe](https://github.com/JordanKnott/taskcafe) - 支持对任务进行筛选过滤、打标签、添加截止日期、分配成员、制定流程等操作。
39. [Reflection Summary](https://github.com/sladesha/Reflection_Summary) - 算法理论基础知识，涵盖了数学、数据预处理、机器学习、深度学习、自然语言处理等多个技术领域的面试知识。
40. [H2O Wave](https://github.com/h2oai/wave) -  仪表面板工具，可用于快速构建实时、低延迟、优雅美观的数据分析仪表面板，适用于 Python 与 R 语言
41. [developer-roadmap](https://github.com/kamranahmedse/developer-roadmap) - 开发者学习路线图，可以为前端、后端等开发者提供一条学习路线路。
42. [30-seconds-of-code](https://github.com/30-seconds/30-seconds-of-code) - 满足您所有开发需求的简短 JavaScript 代码片段。
43. [web-development-resources](https://github.com/markodenic/web-development-resources) - Web 开发资源网站集。
44. [css-protips](https://github.com/AllThingsSmitty/css-protips) - 帮助你提高 CSS 技能专业的技巧集合。
45. [awesome-design-patterns](https://github.com/DovAmir/awesome-design-patterns) - 该库不仅涵盖了通用架构，还包括具体设计模式的代码示例。
47. [awesome-interview-questions](https://github.com/DopplerHQ/awesome-interview-questions) - 针对不同开发者的一份精选面试题，其中包括了多种语言、框架、平台、缓存技术、区块链、数据结构等多个方面的面试内容。
48. [free-programming-books](https://github.com/EbookFoundation/free-programming-books) - 该库包含许多免费的学习资源，你可以从中学习各种技术技能。
49. [the-art-of-command-line](https://github.com/jlevy/the-art-of-command-line) - 该库包含在 Linux 环境中使用命令行的有用建议，同时还包含适合 Windows 或 macOS 系统的内容。

