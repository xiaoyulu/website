# SQL



1. [MySQL 技术论坛](https://learnku.com/mysql)
2. [SQL 教程](https://www.liaoxuefeng.com/wiki/1177760294764384)
3. [mariadb 官方手册](https://mariadb.com/kb/zh-cn/mariadb/)
4. [MySQL 错误码](https://gitee.com/xiaoyulu/website/wikis/?sort_id=2039023)
5. [shardingsphere](http://shardingsphere.apache.org/index_zh.html) - Apache ShardingSphere 是一套开源的分布式数据库解决方案组成的生态圈。
5. [db-tutorial](https://github.com/dunwu/db-tutorial) - 里面含有 MySQL、NoSQL、MongoDB、Elasticsearch、Redis 等学习教程，其中 MySQL 与 Redis 提供了思维导图对学习重点内容进行梳理，以便大家更好学习理解。[gitee](https://gitee.com/turnon/db-tutorial)，[电子书](https://turnon.gitee.io/db-tutorial/)
7. [mysql-tutorial](https://github.com/jaywcjlove/mysql-tutorial) - MySQL入门教程。[21分钟MySQL基础入门](https://github.com/jaywcjlove/mysql-tutorial/blob/master/21-minutes-MySQL-basic-entry.md)