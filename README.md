# 网站

- [国家中小学智慧教育平台](https://www.zxx.edu.cn/) - 汇集了小、中、高三个阶段国家优质资源平台。
- [学习强国](https://www.xuexi.cn/) - 中共中央宣传部主管。【学习慕课, 学习科学(科学辟谣、科普知识、心理辅导)】
- [中国记录片网](http://www.docuchina.cn/) - 央视网旗下的纪录片新媒体平台。
- [云听](http://www.radio.cn/pc-portal/home/index.html) - 是中央广播电视总台推出的总台高品质声音聚合分发平台。
- [中国国家图书馆](http://www.nlc.cn/) - 国家总书库，国家书目中心，国家古籍保护中心。
- [全历史](https://www.allhistory.com/) - 以 AI 知识图谱为核心引擎，通过高度时空化、关联化数据的方式构造及展现数字人文内容。
- [国家智慧教育公共服务平台](https://www.chinaooc.com.cn/) - 国家高等教育智慧教育平台是由教育部委托、高等教育出版社有限公司建设和运行维护、北京理工大学提供技术支持的全国性、综合性在线开放课程平台。
- [国家职业教育智慧教育平台](https://vocational.smartedu.cn/) - 由教育部联合全国各地职业院校创办的网络课程教育平台。
- [国家哲学社会科学文献中心](https://www.ncpssd.org/) - 立足全国哲学社会科学领域，由国家投入和支持，开展哲学社会科学文献信息资源建设和服务平台。
- [国家高等教育智慧教育平台](https://higher.smartedu.cn/) - 是由教育部委托、高等教育出版社有限公司建设和运行维护、北京理工大学提供技术支持的全国性、综合性在线开放课程平台。
- [中国社会科学网](http://www.cssn.cn/)
- [中国医药信息查询平台](https://www.dayi.org.cn/) - 国家权威认证全类型医药信息查询平台。
- [终身教育平台](http://le.ouchn.cn/) - 一个提供免费学习各类课程和资源的国家级网站。
- [中国营养学会](https://www.cnsoc.org/)
- [中国第一历史档案馆](https://www.fhac.com.cn)
- [中国计算机技术职业资格网](https://www.ruankao.org.cn/)
- [广东人事考试网](http://rsks.gd.gov.cn/)
- [中国社会组织政务服务平台](https://chinanpo.mca.gov.cn/)
- [民用无人驾驶航空器综合管理平台](https://uom.caac.gov.cn)
- [航旅纵横](https://www.umetrip.com/webStatic/about/download.html) - 飞机出行必备 app。
- [12321](https://12321.cn/) - 工业和信息化部委托中国互联网协会设立的公众投诉受理机构，负责协助工业和信息化部承担关于互联网、电话网等信息通信网络中的不良与垃圾信息投诉受理、线索转办及信息统计等工作。
- [邮政申诉服务平台](https://sswz.spb.gov.cn/) - 国家邮政局为保障消费者权益和邮政行业发展而设立的官方平台。
- [工业和信息化部](https://www.miit.gov.cn/)
- [国家数据统计局](https://data.stats.gov.cn/) - 中国统计局官方网站提供的一个数据查询平台。
- [中国法律服务网](http://www.12348.gov.cn/) - 司法部主办的法律服务平台，提供法律服务、法律职业资格认证等法律相关服务。
- [国家企业信用信息公示系统](https://www.gsxt.gov.cn) - 一个提供市场主体信用信息的官方平台。
- [合同示范文本库](https://cont.12315.cn/) - 收集了市场监管部门制定发布的各类合同示范文本，供社会公众查阅下载的国家平台。
- [中国裁判文书网](https://wenshu.court.gov.cn/) - 一个提供全国各级法院裁判文书的公开平台。
- [标准地图服务系统](http://211.159.153.75/) - 社会公众可以免费浏览、下载标准地图。
- [国聘](https://www.iguopin.com/) - 国内最大的校园招聘平台。
- [国家药品监督管理局](https://www.nmpa.gov.cn/index.html) - 中央政府的组成部门，负责全国药品、医疗器械、化妆品等相关物品的安全监督。
- [国家医保服务平台](https://m12333.cn/platform/ccs.html) - 国家医保局开发，是国家统一的医保服务平台。
- [PubScholar公益学术平台](https://pubscholar.cn/) - 中国科学院组织院属文献情报中心、网络信息中心、科学出版社等机构，“举全院之力，聚各方资源”建设了公益学术平台。
- [National data国家数据](https://data.stats.gov.cn/staticreq.htm?m=aboutctryinfo) - 国家统计局于2013年建立的新版统计数据库。
- [个人信用信息服务平台](https://ipcrs.pbccrc.org.cn/) - 中国人民银行征信中心建设的基于互联网运行的个人信用信息服务平台。
- [黑猫投诉](https://tousu.sina.com.cn/) - 新浪旗下的一个提供网上投诉、维权、解决问题的平台。
- [中国科普博览](http://v.kepu.net.cn/) - 是中国科学院权威出品、专业打造的互联网科普云矩阵，传播高品质、有深度、可信任的科普内容，组织新鲜、有趣、好玩的科普教育活动，形成中国科普博览、格致论道、科学大院、科院少年等优质科普品牌。
- [阳光志愿](https://gaokao.chsi.com.cn/) - 教育部阳光高考信息平台,高校招生章程,招生政策,高考分数线,网上咨询周,志愿填报。
- [助学贷款学生在线系统](https://sls.cdb.com.cn) - 国家开发银行。
- [黄大年茶思屋](https://www.chaspark.com) - 中国的国际学术平台。【华为】
- [深圳市人力资源生态服务平台](https://hrsspub.sz.gov.cn/szrshreco/pc/#/) - 导航栏点击【找培训】，有免费的学习课程。
- [人民教育出版社](https://www.pep.com.cn/) - 提供权威的汉语教材和教学资源。
- [中华人民共和国海关总署](http://www.customs.gov.cn/) - 查看进口食品不合格：`首页 > 信息服务 > 进口食品安全风险预警 > 未准入境的食品信息`。
- [中国裁判文书网](https://wenshu.court.gov.cn/)
- [中国执行信息公开网](https://zxgk.court.gov.cn/)
- [国家企业信用信息公示系统](https://www.gsxt.gov.cn/index.html)
- [中国人民银行征信中心网](http://www.pbccrc.org.cn/)



#### 介绍

开发使用及涉及到的网站

- [其他](other/)
- [工具](tools/)
- [技术学习](technical-learning)
- [JavaScript](javascript/)
- [SQL](SQL.md)
- [Redis](redis.md)
- [GoLang](golang.md)
- [搜索引擎](search-engines.md)
- [markdown](markdown.md)
- [优秀开源项目](open-source-project.md)



# 法规

- [无人驾驶航空器飞行管理暂行条例](https://www.gov.cn/zhengce/zhengceku/202306/content_6888800.htm)
- [深圳市民用微轻型无人机管理暂行办法](http://www.sz.gov.cn/szsrmzfxxgk/zc/gz/content/post_9452779.html)