# 其他



1. [廖雪峰的官方网站](https://www.liaoxuefeng.com/)
2. [阿里云官方镜像站](https://developer.aliyun.com/mirror/?lang=zh-CN)
3. [书栈网](https://www.bookstack.cn/) - 在线阅读开发手册。
4. [IBM Developer](https://www.ibm.com/developerworks/cn/) - IBM 技术社区。
5. [RequestBin](http://requestbin.net/) - 免费 URL 提供者，可以查看 URL 的请求信息等。
6. [Certbot](https://certbot.eff.org/) - 免费 HTTPS 证书。
7. [阮一峰博客](http://www.ruanyifeng.com/blog/)
8. [博客园](https://www.cnblogs.com/) - 开发者知识分享社区。
9. [程序员导航](http://geekdocs.cn/) -  国内最专业的程序员导航。
10. [风雪之隅](http://www.laruence.com/) - 鸟哥个人技术博客。
11. [黑帽联盟](http://bbs.cnblackhat.com/) - 网络安全技术论坛。
12. [在线抠图网站](https://www.remove.bg/) - 一款抠图的国外在线网站,能够抠除人物背景图片。
13. [Helloweba](https://www.helloweba.net/) - WEB技术分享。
14. [中国知网](https://www.cnki.net/)- 中国知识基础设施工程。
15. [learnku](https://learnku.com/) - 终身编程者的知识社区。
18. [MSDN系统库](https://www.xitongku.com/) - windows 系统下载。
19. [Windows 系统下载存储站](https://hellowindows.cn/) - 含激活工具。
21. [深度 Linux](https://www.deepin.org/zh/)
23. [爱达网站导航](https://adzhp.cn/) - 备用网址 [备用-1](https://adzhp.net/)，[备用-2](https://adzhp.vip/)
20. [银河系漫游指南](https://www.goto-mars.com/people/aLYqyNvYvd) - 导航网站
21. [中国红客联盟](https://www.chinesehongker.com/) - 由 林勇（lion）牵头的中国白帽技术联盟。
22. [知鸟网](http://www.whatbird.cn/) - 鸟类数据字典根据[《中国鸟类野外手册》](http://www.cnbird.org.cn/shouce/c.asp.htm)整理。
23. [代码安全指南 - 腾讯](https://github.com/Tencent/secguide)
24. [测试之家](https://testerhome.com/) - 由一线测试工程师发起和运营，社区的主旨是公益，开源，分享，落地。
25. [开放原子开源基金会](https://www.openatom.org/)
26. [开源麒麟](https://www.openkylin.top/) - 国产操作系统。[gitee](https://gitee.com/openkylin)
27. [优麒麟](https://www.ubuntukylin.com/) - 国产操作系统。[gitee](https://gitee.com/ukylin-os)
28. [GitHub镜像网站](https://hub.nuaa.cf/)，[镜像网站2](https://hub.yzuu.cf/)
29. [面试鸭](https://www.mianshiya.com/) - 面试题库。

